const nextRoutes = require("next-routes");
const routes = nextRoutes();

routes.add("home", "/");
routes.add("login", "/login");
routes.add("signup", "/signup");
routes.add("favorites", "/favorites");
routes.add("faq", "/faq");

module.exports = routes;
