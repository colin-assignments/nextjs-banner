export function isEmail(email) {
  const emailPattern = /^[A-Z0-9._+-]{3,}@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;
  return emailPattern.test(email);
}

export function isValidStrLength(str, minLength) {
  return str && str.length >= minLength;
}

export function checkRequiredFields(fields, values, msg = "") {
  const errors = {};

  if (!msg) {
    msg = "Please fill in your";
  }

  fields.forEach(field => {
    if (!values[field]) {
      errors[field] = msg + " " + field;
    }
  });

  return errors;
}
