export const colors = {
  primaryColor: "#142158",
  secondaryColor: "#5063F0",
  secondaryColorDark: "#1F3BDF",

  primary: '#043353',
  primaryLight: '#3a5c80',
  primaryDark: '#000b2a',

  secondary: '#18a4e0',
  secondaryLight: '#67d5ff',
  secondaryDark: '#0075ae',

  grayText: "#999999",
  backgroundColor: "#F2F2F2",
  borderColor: "rgba(0, 0, 0, 0.23)",
  footerColor: "#F5F5F5",
  black: "#3b4144",
  white: "#FFFFFF",
  red: "#C6292E",
  green: "#205814",
  textColor: "#3b4144",
  textColorSecondary: "#869099",
  borderColorSecondary: "#f7f7f7"
};

export const transitionDuration = 200;

export const navbarHeight = 65;

export const drawerWidth = 300;

export const planList = [
  {
    key: 0,
    planID: 'price_1H399fEc1F8Yz5UfncQnUoOX',
    planName: 'Basic',
    price: '28',
    duration: 'Month',
    billed: 'Monthly',
  },
  {
    key: 1,
    planID: 'price_1I31QoEc1F8Yz5UfgrP4bIuX',
    planName: 'Premium',
    price: '46',
    duration: 'Month',
    billed: 'Monthly',
  },
  {
    key: 2,
    planID: 'price_1I31QoEc1F8Yz5Ufg5ZvnKsU',
    planName: 'Basic Yearly',
    price: '220',
    duration: 'Year',
    billed: 'Yearly',
  },
  {
    key: 3,
    planID: 'price_1I31QoEc1F8Yz5UfG4pIphDe',
    planName: 'Premium Yearly',
    price: '340',
    duration: 'Year',
    billed: 'Yearly',
  },
];

export const newPlanList = [
  {
    key: 0,
    planID: 'price_1H399fEc1F8Yz5UfncQnUoOX',
    planName: 'Basic',
    price: '28',
    duration: 'Month',
    billed: 'Monthly',
    savedPercent: 0,
    beforeDiscounted: '28',
    description: '$28 billed every month',
    isPopular: false,
  },
  {
    key: 1,
    planID: 'price_1I31QoEc1F8Yz5UfgrP4bIuX',
    planName: 'Premium',
    price: '46',
    duration: 'Month',
    billed: 'Monthly',
    savedPercent: 0,
    beforeDiscounted: '46',
    description: '$46 billed every month',
    isPopular: true,
  },
  {
    key: 2,
    planID: 'price_1I31QoEc1F8Yz5Ufg5ZvnKsU',
    planName: 'Basic Yearly',
    price: '220',
    duration: 'Year',
    billed: 'Yearly',
    savedPercent: 35,
    beforeDiscounted: '336',
    description: '$220 billed for the first year',
    isPopular: false,
  },
  {
    key: 3,
    planID: 'price_1I31QoEc1F8Yz5UfG4pIphDe',
    planName: 'Premium Yearly',
    price: '340',
    duration: 'Year',
    billed: 'Yearly',
    savedPercent: 38,
    beforeDiscounted: '552',
    description: '$340 billed for the first year',
    isPopular: true,
  },
];

export const planBenefits = [
  { id: 1, name: 'See all new material', basic: true, premium: true },
  {
    id: 2,
    name: 'Fast order material',
    basic: true,
    premium: true,
  },
  {
    id: 3,
    name: 'Create one integration',
    basic: true,
    premium: true,
  },
  {
    id: 4,
    name: 'Search the material quickly',
    basic: true,
    premium: true,
  },
  {
    id: 5,
    name: 'See the discover list',
    basic: true,
    premium: true,
  },
  {
    id: 6,
    name: 'Create favourite list',
    basic: false,
    premium: true,
  },
  {
    id: 7,
    name: 'Create unlimited integration',
    basic: false,
    premium: true,
  },
];
