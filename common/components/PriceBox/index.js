import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    width: 300,
    margin: "30px 10px",
    textAlign: "center",
  },
  content: {
    padding: "0px !important",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    padding: "20px",
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
    background: "#142158",
    letterSpacing: "0.9px",
    textTransform: "uppercase",
  },
  price: {
    marginTop: 20,
    fontSize: "1.3rem",
    textAlign: "center",
    margin: "auto",
  },
  duration: {
    marginTop: 10,
  },
  footer: {
    justifyContent: "center",
    margin: "10px 0 20px 0",
  },
  btnSelect: {
    fontSize: 16,
    letterSpacing: "0.9px",
    border: "2px solid #142158",
    "&:hover": {
      background: "white",
      border: "2px solid #142158",
      color: "#142158",
    },
  },
});

const PriceBox = (props) => {
  const classes = useStyles();
  const { plan:{planName, price, duration, billed}, setPlan, setActive } = props;
  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <Typography className={classes.title} color="textSecondary">
          {planName}
        </Typography>
        <Typography variant="h5" className={classes.price} component="h2">
          <sup>$ </sup>
          {price} / {duration}
        </Typography>

        <Typography variant="body2" component="p" className={classes.duration}>
          {billed}
        </Typography>
      </CardContent>
      <CardActions className={classes.footer}>
        <Button
          type="submit"
          variant="contained"
          className={classes.btnSelect}
          color="primary"
          onClick={() => {
            setActive(true);
            setPlan(props.plan);
          }}
        >
          Select
        </Button>
      </CardActions>
    </Card>
  );
};

export default PriceBox;
