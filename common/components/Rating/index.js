import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import MuiRating from '@material-ui/lab/Rating';
import FiberManualRecordSharpIcon from '@material-ui/icons/FiberManualRecordSharp';

const styles = theme => ({
  iconFilled: {
    color: theme.palette.secondary.dark,
  },
  iconHover: {
    color: '#038f52'
  }
});

class Rating extends Component {
  render() {
    return <MuiRating icon={<FiberManualRecordSharpIcon />} {...this.props} />;
  }
}

Rating.defaultProps = {};

Rating.propTypes = {};

export default withStyles(styles)(Rating);
