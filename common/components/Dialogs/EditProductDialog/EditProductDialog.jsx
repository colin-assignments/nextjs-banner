import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';

import { compose } from 'redux';
import styles from './styles';
import { withStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {
  Divider,
  Button,
  CircularProgress,
  DialogContent,
  DialogTitle,
  Dialog,
} from '@material-ui/core';
import { Block } from '@material-ui/icons';

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  
  switchBase: {
    padding: 1,
    "& .MuiSwitch-thumb":{
     
    },
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: theme.palette.primary.main,
        // backgroundColor: '#52d869',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
    '&:before': {
      color: theme.palette.primary.main,
    },
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});
class EditProductDialog extends Component {
  state = {
    checked:true,
  };

   handleChange = (event) => {
    this.setState({checked: event.target.checked });
  };
  handleSave = () => {};

  render() {
    const { classes, isOpen, handleClose } = this.props;
    return (
      <If condition={isOpen}>
        <Dialog
          className={classes.popup}
          onClick={this.preventClick}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={isOpen}
        >
          <DialogTitle id="form-dialog-title" className={classes.title}>
            <div className={classes.dialogTitle}>
              <div>Edit product</div>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={handleClose}
              >
                <CloseIcon />
              </IconButton>
            </div>
          </DialogTitle>
          <Divider />
          <DialogContent className={classes.dialogContent}>
            <div className={classes.contentWrap}>
            <FormControlLabel
              control={<IOSSwitch checked={this.state.checked} onChange={this.handleChange} name="checked" />}
              label="iOS style"
            />
            </div>
            <Divider />
            <div className={classes.actionsGroup}>
              <Button
                variant="outlined"
                className={classes.cancelBtn}
                onClick={handleClose}
              >
                Cancel
              </Button>
              <Button
                autoFocus
                className={classes.saveBtn}
                onClick={this.handleSave}
              >
                <If condition={this.state.isSaving}>
                  <CircularProgress className={classes.spinIcon} size={15} />
                </If>
                Save
              </Button>
            </div>
          </DialogContent>
        </Dialog>
      </If>
    );
  }
}

EditProductDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default compose(withStyles(styles))(EditProductDialog);
