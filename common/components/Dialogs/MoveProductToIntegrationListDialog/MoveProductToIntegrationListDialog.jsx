import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Checkbox,
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

class MoveProductToIntegrationListDialog extends Component {
  state = {
    selectedIntegrations: [],
    productIntegrations: [],
    isSaving: false,
    isLoading: false,
  };

  render() {
    const { classes, isOpen, handleClose, pages } = this.props;

    return (
      <If condition={isOpen}>
        <Dialog
          className={classes.popup}
          onClick={this.preventClick}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={isOpen}
        >
          <DialogTitle id="form-dialog-title" className={classes.title}>
            <div className={classes.dialogTitle}>
              <div>Save to a list</div>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={handleClose}
              >
                <CloseIcon />
              </IconButton>
            </div>
          </DialogTitle>
          <Divider />
          <DialogContent className={classes.dialogContent}>
            <div className={classes.contentWrap}>
              <div>
                Choose which of the lists you would like to place your favorite
                property
              </div>
              <div>
                <List disablePadding className={classes.nestedList}>
                  {pages.map((integration) => {
                    return (
                      <ListItem
                        key={integration.id}
                        role={undefined}
                        dense
                        button
                      // onClick={this.handleToggleSelect(integration.code)}
                      >
                        <ListItemIcon>
                          <Checkbox
                            edge="start"
                            tabIndex={-1}
                            disableRipple
                          // checked={this.isSelected(integration.code)}
                          />
                        </ListItemIcon>
                        <ListItemText
                          id={integration.id}
                          primary={integration.code}
                        />
                      </ListItem>
                    );
                  })}
                </List>
                {/* <Choose>
                  <When condition={this.state.isLoading}>
                    <div className={classes.loadingIconWrap}>
                      <CircularProgress
                        className={classes.loadingIcon}
                        size={20}
                      />
                    </div>
                  </When>
                  <Otherwise>
                    <List disablePadding className={classes.nestedList}>
                      {pages.map((integration) => {
                        return (
                          <ListItem
                            key={integration.id}
                            role={undefined}
                            dense
                            button
                            // onClick={this.handleToggleSelect(integration.code)}
                          >
                            <ListItemIcon>
                              <Checkbox
                                edge="start"
                                tabIndex={-1}
                                disableRipple
                                // checked={this.isSelected(integration.code)}
                              />
                            </ListItemIcon>
                            <ListItemText
                              id={integration.id}
                              primary={integration.code}
                            />
                          </ListItem>
                        );
                      })}
                    </List>
                  </Otherwise>
                </Choose> */}
              </div>
            </div>
            <Divider />
            <div className={classes.actionsGroup}>
              <Button
                variant="outlined"
                className={classes.createListBtn}
                onClick={this.handleCreateNewIntegration}
              >
                Create new list
              </Button>
              <Button
                autoFocus
                className={classes.saveBtn}
                onClick={this.handleSave}
              >
                <If condition={this.state.isSaving}>
                  <CircularProgress className={classes.spinIcon} size={15} />
                </If>
                Save
              </Button>
            </div>
          </DialogContent>
        </Dialog>
      </If>
    );
  }
}

MoveProductToIntegrationListDialog.defaultProps = {
  pages: [
    {
      id: "whatsapp",
      code: "whatsapp"
    },
    {
      id: "slack",
      code: "slack"
    },
    {
      id: "google",
      code: "google"
    },
  ],
};

MoveProductToIntegrationListDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  product: PropTypes.any.isRequired,
  getIntegrationPages: PropTypes.func.isRequired,
  getProductIntegrations: PropTypes.func.isRequired,
  pages: PropTypes.array,
  addProductToIntegration: PropTypes.func.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
};

// export default compose(
//   connect(mapStateToProps, mapDispatchToProps),
//   withStyles(styles),
// )(MoveProductToIntegrationListDialog);
export default withStyles(styles)(MoveProductToIntegrationListDialog);