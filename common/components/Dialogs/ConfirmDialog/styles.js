const styles = (theme) => ({
  actions: {
    padding: '10px'
  },
  cancelBtn: {
    backgroundColor: '#757575',
    color: 'white',

    '&:hover': {
      backgroundColor: '#bdbdbd',
    }
  },
  okBtn: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',

    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    }
  },
  btn: {
    textTransform: 'none',
    padding: '5px 20px'
  }
});

export default styles;
