import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import cs from 'classnames';

import {
  Paper,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';

import styles from './styles';
import withStyles from '@material-ui/core/styles/withStyles';

class ConfirmBox extends Component {
  render() {
    const {
      classes,
      handleOk,
      title,
      cancelName,
      okName,
      content,
      open,
      handleCancel,
    } = this.props;

    return (
      <div>
        <Dialog
          open={open}
          onClose={handleCancel}
          PaperComponent={Paper}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            {title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>{content}</DialogContentText>
          </DialogContent>
          <DialogActions className={classes.actions}>
            <Button autoFocus onClick={handleCancel} className={cs(classes.cancelBtn, classes.btn)}>
              {cancelName}
            </Button>
            <Button onClick={handleOk} className={cs(classes.okBtn, classes.btn)}>
              {okName}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
ConfirmBox.defaultProps = {
  title: 'Confirm',
  open: false,
  cancelName: 'Cancel',
  okName: 'OK',
  content: '',
};

ConfirmBox.propTypes = {
  title: PropTypes.string,
  open: PropTypes.bool,
  cancelName: PropTypes.string,
  okName: PropTypes.string,
  handleOk: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
  content: PropTypes.string,
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles))(ConfirmBox);
