import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import { colors } from 'common/constants';
import FavoriteIcon from '@material-ui/icons/Favorite';

import { displayRegisterDialog } from 'common/store/actions';

const useStyles = makeStyles((theme) => ({
  likedButton: {
    color: colors.red,
    padding: theme.spacing(1),
    marginLeft: theme.spacing(2),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function RegisterDialogSlide(props) {
  const classes = useStyles();

  const handleClose = () => {
    props.displayRegisterDialog(false);
  };

  function handleCreateAccount() {
    props.displayRegisterDialog(false);
    Router.push('/auth/signup');
  }

  return (
    <div>
      <Dialog
        open={props.registerDialogDisplay}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.likedButton} size="medium">
          <FavoriteIcon />
        </div>
        <DialogTitle id="alert-dialog-slide-title">
          {'You favorited this product!'}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            To access your favorites and track your products from anywhere,
            create your account.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" color="secondary" onClick={handleClose}>
            Not now
          </Button>
          <Button
            variant="outlined"
            color="primary"
            onClick={handleCreateAccount}
          >
            Create an account
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state) => ({
  registerDialogDisplay: state.ui.registerDialogDisplay,
});

const mapDispatchToProps = (dispatch) => {
  return {
    displayRegisterDialog: bindActionCreators(displayRegisterDialog, dispatch),
  };
};

RegisterDialogSlide.defaultProps = {
  displayRegisterDialog: PropTypes.func,
  registerDialogDisplay: PropTypes.func,
};

RegisterDialogSlide.propTypes = {
  displayRegisterDialog: PropTypes.func,
  registerDialogDisplay: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterDialogSlide);
