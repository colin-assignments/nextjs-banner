import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { compose } from 'redux';
import styles from './styles';
import cs from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {
  Divider,
  Button,
  DialogContent,
  Dialog,
  Container,
} from '@material-ui/core';

import { newPlanList } from 'common/constants';
import { Elements } from '@stripe/react-stripe-js';
import CheckoutForm from '../../Forms/CheckoutForm';
import { loadStripe } from '@stripe/stripe-js';
import { connect } from 'react-redux';

const stripePromise = loadStripe('pk_test_zerKb9Lm2SoxfcrdjM18XAR800igjb3dKC');

class SubscriptionDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSaving: false,
      currentMembership: "Basic",
      planSelected: props.planSelected,
    };
  }

  render() {
    const { classes, isOpen, handleClose } = this.props;
    const { currentMembership } = this.state;
    return (
      <If condition={isOpen}>
        <Dialog
          className={classes.popup}
          onClick={this.preventClick}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={isOpen}
          disableBackdropClick={true}
        >
          <Divider />
          <DialogContent className={classes.dialogContent}>
            <div className={classes.dialogTitle}>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={handleClose}
              >
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.contentWrap}>
              <div className={classes.membershipSwitch}>
                <Button
                  className={cs(classes.membershipSwitchItem, {
                    [classes.selectedSwitch]: currentMembership === 'Basic',
                  })}
                  // onClick={this.handleChangeMembership('Basic')}
                >
                  Basic Membership
                </Button>
                <Button
                  className={cs(classes.membershipSwitchItem, {
                    [classes.selectedSwitch]: currentMembership === 'Premium',
                  })}
                  // onClick={this.handleChangeMembership('Premium')}
                >
                  Premium Membership
                </Button>
              </div>

              <div className={classes.planList}>
                <Button
                  className={cs(classes.planItem, {
                    [classes.selectedPlan]: {},
                  })}
                  // key={plan.key}
                  // onClick={this.handleSelectPlan(plan)}
                >
                  {/* {plan.billed}: ${plan.price}/{plan.duration} */}
                  12.99
                  <div
                    className={cs(classes.planDescription, {
                      [classes.selectedDescription]: {},
                    })}
                  >
                    {"plan.description"}
                  </div>
                </Button>
                <Button
                  className={cs(classes.planItem)}
                  // key={plan.key}
                  // onClick={this.handleSelectPlan(plan)}
                >
                  {/* {plan.billed}: ${plan.price}/{plan.duration} */}
                  1200.99
                  <div
                    className={cs(classes.planDescription)}
                  >
                    {"plan.description"}
                  </div>
                </Button>
                {/* {newPlanList
                  .filter((item) => item.planName.includes(currentMembership))
                  .map((plan) => (
                    <Button
                      className={cs(classes.planItem, {
                        [classes.selectedPlan]: this.isSelected(plan),
                      })}
                      key={plan.key}
                      onClick={this.handleSelectPlan(plan)}
                    >
                      {plan.billed}: ${plan.price}/{plan.duration}
                      <div
                        className={cs(classes.planDescription, {
                          [classes.selectedDescription]: this.isSelected(plan),
                        })}
                      >
                        {plan.description}
                      </div>
                    </Button>
                  ))} */}
              </div>

              <Container className={classes.container}>
                <Elements stripe={stripePromise}>
                  <CheckoutForm
                    onSubmit={this.onSubmit}
                    isSubmiting={this.state.isSaving}
                    planText="premium"
                  />
                </Elements>
              </Container>
            </div>
          </DialogContent>
        </Dialog>
      </If>
    );
  }
}

SubscriptionDialog.defaultProps = {
  planSelected: {},
};

SubscriptionDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  planSelected: PropTypes.object,
  enqueueSnackbar: PropTypes.func.isRequired,
  addUserSubscription: PropTypes.func.isRequired,
  updateUserSubscription: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

// export default compose(
//   connect(null, null),
//   withStyles(styles),
// )(SubscriptionDialog);

export default withStyles(styles)(SubscriptionDialog);