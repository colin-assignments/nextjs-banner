const styles = (theme) => ({
  popup: {
    width: '100%',
    ' & .MuiDialog-paper': {
      maxWidth: '500px',
      [theme.breakpoints.down('xs')]: {
        width: '100%',
        margin: '15px',
      },
    },
    position: 'relative',
  },
  title: {
    padding: '10px 24px',

    [theme.breakpoints.down('xs')]: {
      padding: '10px 15px',
    },
  },
  dialogContent: {
    maxWidth: '500px',
    minWidth: '400px',
    width: '100%',
    padding: 0,

    [theme.breakpoints.down('xs')]: {
      minWidth: '100%',
    },
  },
  contentWrap: {
    padding: '24px',
  },
  dialogTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  closeButton: {
    padding: '3px',
    position: 'absolute',
    right: '5px',
    top: '5px',
  },
  cancelBtn: {
    textTransform: 'none',
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: 'rgb(233, 233, 253)',
    },
  },
  saveBtn: {
    textTransform: 'none',
    color: 'white',
    backgroundColor: theme.palette.primary.main,
    marginLeft: '10px',

    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  actionsGroup: {
    padding: '16px 24px',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  spinIcon: {
    color: 'white',
    marginRight: '10px',
  },
  loadingIcon: {
    color: theme.palette.primary.main,
  },
  loadingIconWrap: {
    display: 'flex',
    justifyContent: 'center',
    margin: '20px',
  },
  membershipSwitch: {
    width: '100%',
    marginTop: '10px',
  },
  membershipSwitchItem: {
    width: '50%',
    borderRadius: '6px',
    textTransform: 'capitalize',
    color: theme.palette.primary.main,
    background: '#ddd',
    '&:hover': {
      background: '#ddd',
      transform: 'scale(1, 1.2)',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '13px'
    },
  },
  selectedSwitch: {
    background: theme.palette.primary.main,
    transform: 'scale(1, 1.3)',
    color: '#fff',
    zIndex: '100',
    '&:hover': {
      background: theme.palette.primary.main,
    },
  },
  planList: {
    margin: '15px 0',
    borderRadius: '10px',
  },
  planItem: {
    width: '100%',
    cursor: 'pointer',
    border: '1px solid #ddd',
    borderBottom: '0px',
    padding: '10px',
    backgroundColor: 'rgb(233, 233, 253)',
    textAlign: 'center',
    '&:first-child': {
      borderRadius: '8px 8px 0 0',
    },
    '&:last-child': {
      borderRadius: '0px 0px 8px 8px',
      borderBottom: '1px solid #ddd',
    },
    '&:hover': {
      backgroundColor: 'rgb(233, 233, 253)',
    },
    display: 'block',
    textTransform: 'none'
  },
  planDescription: {
    fontSize: '10px',
    color: 'gray',
  },
  selectedPlan: {
    background: theme.palette.primary.main,
    color: '#fff',
    transform: 'scale(1.04, 1.1)',
    borderRadius: '8px !important',
    '&:hover': {
      background: theme.palette.primary.main,
    },
    zIndex: '100'
  },
  selectedDescription: {
    color: '#dfdfdf'
  },
  container: {
    padding: 0
  }
});

export default styles;
