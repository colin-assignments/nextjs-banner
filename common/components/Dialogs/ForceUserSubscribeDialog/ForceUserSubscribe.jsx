import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'common/routes';

import styles from './styles';
import { makeStyles } from '@material-ui/core/styles';
import { planBenefits } from 'common/constants';

import {
  Button,
  Dialog,
  DialogContent,
  Divider,
  Grid,
} from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const useStyles = makeStyles(styles);


const ForceUserSubscribe = (props) => {
  const classes = useStyles();
  const { isOpen } = props;

  const preventClick = (evt) => {
    evt.stopPropagation();
    evt.preventDefault();
  };

  return (
    <div>
      <If condition={isOpen}>
        <Dialog
          className={classes.popup}
          onClick={preventClick}
          aria-labelledby="customized-dialog-title"
          open={isOpen}
        >
          <DialogContent className={classes.dialogContent}>
            <div className={classes.contentWrap}>
              <div className={classes.title}>Subscribe to continue with us</div>
              <Grid container spacing={2}>
                <Grid item xs={12} lg={6} sm={6}>
                  <div className={classes.item}>
                    <div className={classes.planTitle}>Basic Membership</div>
                    <div className={classes.price}>
                      <sup>$</sup>28/<span className={classes.duration}>month</span> or <sup>$</sup>220/<span className={classes.duration}>year</span>
                    </div>
                    <Divider/>
                    <div className={classes.benefits}>
                      {planBenefits.map((item, index) => (
                        <div key={index} className={classes.benefitItem}>
                          <Choose>
                            <When condition={item.basic}>
                              <CheckCircleIcon className={classes.checkIcon}/>
                            </When>
                            <Otherwise>
                              <HighlightOffIcon className={classes.offIcon}/>
                            </Otherwise>
                          </Choose>
                          <div className={classes.benefitName}>{item.name}</div>
                        </div>
                      ))}
                    </div>
                  </div>

                </Grid>
                <Grid item xs={12} lg={6} sm={6}>
                  <div className={classes.item}>
                    <div className={classes.planTitle}>Premium Membership</div>
                    <div className={classes.price}>
                      <sup>$</sup>46/<span className={classes.duration}>month</span> or <sup>$</sup>340/<span className={classes.duration}>year</span>
                    </div>
                    <Divider />
                    <div  className={classes.benefits}>
                      {planBenefits.map((item, index) => (
                        <div key={index} className={classes.benefitItem}>
                          <Choose>
                            <When condition={item.premium}>
                              <CheckCircleIcon className={classes.checkIcon}/>
                            </When>
                            <Otherwise>
                              <HighlightOffIcon className={classes.offIcon}/>
                            </Otherwise>
                          </Choose>
                          <div className={classes.benefitName}>{item.name}</div>
                        </div>
                      ))}
                    </div>
                  </div>
                </Grid>
              </Grid>
              <div className={classes.actionsGroup}>
                <Link to="/subscription">
                  <Button autoFocus className={classes.goToBtn}>
                    Subscribe Now
                  </Button>
                </Link>
              </div>
            </div>
          </DialogContent>
        </Dialog>
      </If>
    </div>
  );
};

ForceUserSubscribe.defaultProps = {
  isOpen: false,
};

ForceUserSubscribe.propTypes = {
  isOpen: PropTypes.bool,
};

export default ForceUserSubscribe;
