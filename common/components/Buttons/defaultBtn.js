import React from "react";
import styled from "styled-components"
import { SButton } from './components'

const StyledButton = styled(SButton)`
    background: transparent;
    color: rgba(0,0,0,0.6) !important;
    border:1px solid rgba(0,0,0,0.3) !important;
    svg{
        font-size:25px !important;
    }
    :hover{
        background: transparent;
        color:rgba(0,0,0,0.7);
        border:1px solid rgba(0,0,0,0.7);

    }
    :focus{
      background: rgb(233, 233, 253) !important;
      border: 1px solid rgb(233, 233, 253) !important;
      color:rgb(31, 59, 223) !important;
    }
`
const PrimaryButton = (props) => {
  return (
    <StyledButton {...props} startIcon={props.icon} icon={props.icon}>{props.children}</StyledButton>
  )
}

export default PrimaryButton;
