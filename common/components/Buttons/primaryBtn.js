import React from "react";
import styled from "styled-components"
import { SButton } from './components'

const StyledButton = styled(SButton)`
    background: #5063f0 !important;
    color:white !important;
    :hover{
        background: #1F3BDF !important;
        color:white !important;
        box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12) !important;

    }
`
const PrimaryButton = (props) => {
  return (
    <StyledButton {...props}>{props.children}</StyledButton>
  )
}

export default PrimaryButton;