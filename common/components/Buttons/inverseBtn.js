import React from "react";
import styled from "styled-components"
import { SButton } from './components'

const StyledButton = styled(SButton)`
    background: transparent !important;
    color:#5063f0 !important;
    border:1px solid #5063f0 !important;
    :hover{
        background: transparent !important;
        color:#1F3BDF !important;
        border:1px solid #1F3BDF !important;

    }
`
const PrimaryButton = (props) => {
  return (
    <StyledButton {...props}>{props.children}</StyledButton>
  )
}

export default PrimaryButton;
