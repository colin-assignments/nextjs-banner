import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Avatar,
  Box,
  Menu,
  List,
  ListItem,
  Divider,
  IconButton,
  SvgIcon,
  Grid,
} from "@material-ui/core";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import FavoriteIcon from "@material-ui/icons/Favorite";
import StoreIcon from "@material-ui/icons/Store";
import HomeIcon from "@material-ui/icons/Home";
import EventNoteIcon from "@material-ui/icons/EventNote";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const StyledMenu = withStyles({
  paper: {
    position: "absolute",
    border: "1px solid #d3d4d5",
    display: "block",
    transition: "visibility .25s, opacity .25s",
    boxShadow:
      "0px 8px 11px -5px rgba(0,0,0,0.2), 0px 17px 26px 2px rgba(0,0,0,0.14), 0px 6px 32px 5px rgba(0,0,0,0.12)",
    opacity: 0,
    outline: "none !important",
    opacity: 1,
    float: "left",
    borderRadius: "8px !important",
    overflowY: "auto !important",
    zIndex: "2 !important",
    minWidth: "230px !important",
    width: "360px",
    background: "rgb(54, 182, 126) !important",
    padding: "20px",
    color: "white",
    "& .userInfo": {
      marginLeft: "15px",
    },
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledListItem = withStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    appearance: "none !important",
    border: "0px !important",
    color: "white !important",
    cursor: "pointer !important",
    fontSize: "14px !important",
    fontWeight: "500 !important",
    width: "100%",
    height: "80px",
    margin: "0 !important",
    textAlign: "center !important",
    background: "rgb(0, 120, 68) !important",
    borderRadius: "5px",
  },
}))(ListItem);
const StyledListItemBlock = withStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    appearance: "none !important",
    border: "0px !important",
    color: "white !important",
    cursor: "pointer !important",
    fontSize: "14px !important",
    fontWeight: "500 !important",
    width: "100%",
    minHeight: "50px",
    margin: "0 !important",
    textAlign: "center !important",
    background: "rgb(0, 120, 68) !important",
    borderRadius: "5px",
    "& span": {
      marginLeft: "10px",
    },
    "& p": {
      textAlign: "left",
      fontWeight: "400",
    },
    "& strong": {
      fontSize: "16px",
    },
  },
}))(ListItem);

const StyledList = withStyles((theme) => ({
  root: {
    padding: "0 !important",
    "& .list-wrapper": {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
    },
    "& :focus":{
      outline:"none !important"
    }
  },
}))(List);
const StyledBox = withStyles((theme) => ({
  root: {
    padding: "0 15px 20px 0 !important",
    cursor:"pointer",
    display: "flex",
    "& .MuiAvatar-colorDefault": {
      background: "rgb(0, 120, 68) !important",
    },
    "& .title": {
      fontSize: "16px",
    },
    "& span": {
      fontSize: "16px",
      fontWeight: "700",
    },
  },
}))(Box);

export default function HeaderUserbox(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Fragment>
      <IconButton aria-label="menuButton" color="inherit" onClick={handleClick}>
        <SvgIcon>
          <path d="M4 8h4V4H4v4zm6 12h4v-4h-4v4zm-6 0h4v-4H4v4zm0-6h4v-4H4v4zm6 0h4v-4h-4v4zm6-10v4h4V4h-4zm-6 4h4V4h-4v4zm6 6h4v-4h-4v4zm0 6h4v-4h-4v4z" />
        </SvgIcon>
      </IconButton>

      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <div className="dropdown-menu">
          <StyledList className="text-left bg-transparent d-flex align-items-center flex-column pt-0">
            <StyledBox onClick={props.setOpen}>
              <Avatar sizes="44" alt="Emma Taylor" />
              <div className="userInfo">
                <div className="title">Colin Bennet</div>
                <span>Software Developer</span>
              </div>
            </StyledBox>
            <div className="list-wrapper">
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={6}>
                  <StyledListItem button>
                    <AccountBoxIcon />
                    <span>Account</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={6}>
                  <StyledListItem button>
                    <FavoriteIcon />
                    <span>Saved</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={6}>
                  <StyledListItem button>
                    <StoreIcon />
                    <span>Store</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={6}>
                  <StyledListItem button>
                    <HomeIcon />
                    <span>Purchase</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={6}>
                  <StyledListItem button>
                    <MonetizationOnIcon />
                    <span>Proposals</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={6}>
                  <StyledListItem button>
                    <EventNoteIcon />
                    <span>Visits</span>
                  </StyledListItem>
                </Grid>
                <Grid item xs={12}>
                  <StyledListItemBlock button>
                    <p>
                      <strong>
                        Your link is worth a discount for the owner and money
                        for you!
                      </strong>
                      <br />
                      Share the link below for owners to register their
                      properties for rental . They get a discount and you get R
                      $ 100 for the rental publication and 10% of the first
                      rental when it occurs.
                    </p>
                  </StyledListItemBlock>
                </Grid>
                <Grid item xs={12}>
                  <StyledListItemBlock button>
                    <EventNoteIcon />
                    <span>Help</span>
                  </StyledListItemBlock>
                </Grid>
                <Grid item xs={12}>
                  <StyledListItemBlock
                    button
                    onClick={() => props.handleLogout()}
                  >
                    <ExitToAppIcon />
                    <span>Log out</span>
                  </StyledListItemBlock>
                </Grid>
              </Grid>
            </div>
          </StyledList>
        </div>
      </StyledMenu>
    </Fragment>
  );
}
