import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@material-ui/core";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import GroupIcon from "@material-ui/icons/Group";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import LocalActivityIcon from "@material-ui/icons/LocalActivity";

const useStyles = makeStyles((theme) => ({
  subTitle: {
    margin: 0,
    color: "rgba(0,0,0,0.7)",
    fontWeight: 400,
  },

  divider: {
    margin: "10px 0",
  },
  icon: {
    color: "rgba(0,0,0,0.9)",
    "& svg": {
      fontSize: "1.8rem",
    },
  },


}));

export default function AccountInfo(props) {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const links = [
    {
      icon: <LocalActivityIcon />,
      text: "Tenant or Buyer",
    },
    {
      icon: <LocalOfferIcon />,
      text: "Owner",
    },
    {
      icon: <GroupIcon />,
      text: "Indicates",
    },
    {
      icon: <AssignmentIndIcon />,
      text: "Broker",
    },
  ];
  return (
    <Dialog
      onClose={props.onClose}
      aria-labelledby="simple-dialog-title"
      open={props.open}
      maxWidth={"xs"}
      fullWidth
    >
      <DialogTitle id="simple-dialog-title">Profile</DialogTitle>
      <DialogContent>
        <h3 className={classes.subTitle}>Change profile</h3>
        <List>
          {links.map((link, index) => (
            <ListItem
              button
              selected={selectedIndex === index}
              onClick={(event) => handleListItemClick(event, index)}
            >
              <ListItemIcon className={classes.icon}>{link.icon}</ListItemIcon>
              <ListItemText primary={link.text} />
            </ListItem>
          ))}
          <Divider className={classes.divider} />
          <ListItem autoFocus button>
            <ListItemText primary="My Account" />
          </ListItem>
          <ListItem
            autoFocus
            button
            onClick={() => {props.handleLogout(),props.onClose()}}
          >
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </DialogContent>
    </Dialog>
  );
}
