import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import { Link } from "../../../../common/routes";

const styles = theme => ({
  image: {
    marginTop: "5px",
    cursor: "pointer",
    [theme.breakpoints.up("md")]: {
      height: 22
    },
    [theme.breakpoints.down("sm")]: {
      height: 19
    }
  }
});

const Logo = props => {
  const { classes } = props;

  return (
    <Link to="/">
      <img
        src="/static/images/dropseeker-logo.png"
        className={classes.image}
        alt="Logo"
      />
    </Link>
  );
};

export default withStyles(styles)(Logo);
