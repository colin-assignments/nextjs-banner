import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import { colors } from '../../../../common/constants';

const lightColor = 'rgba(255, 255, 255, 0.7)';

const useStyles = makeStyles(theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: theme.spacing(-1)
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  login: {
    padding: "8px",
    fontWeight: 500,
    fontSize: "14px",
    lineHeight: "21px"
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  avatar: {
    fontSize: 16,
    width: "30px",
    height: "30px",
    color: colors.green,
    backgroundColor: colors.white
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const { user } = props;

  const handleMobileMenu = () => {
    displayNavBar(!navBarDisplay);
  };

  return (
    <React.Fragment>
      <AppBar color="primary" elevation={0}>
        <Toolbar>
          <Grid container spacing={1} alignItems="center">
            <Grid item xs>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}