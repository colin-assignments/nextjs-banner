import React, { Component } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import Head from "next/head";
import Router from "next/router";
import NProgress from "nprogress";
import { GlobalStyle } from "./nprogressStyles";

import Header from './Header'


Router.events.on('routeChangeStart', url => {
  console.log(`Loading: ${url}`)
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

const styles = theme => ({
  root: {
    backgorund:"white",
    paddingTop: 56,
    height: "100%",
    [theme.breakpoints.up("sm")]: {
      paddingTop: 64
    },
    [theme.breakpoints.down("sm")]: {
      padding: "0 !important"
    },
   
  },
  shiftContent: {
    paddingLeft: 240
  },
  main: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    minHeight: "100vh"
  },
 
  
});

const defaultTitle = "";
const defaultTitleTagLine = "";

class Page extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, children, title, navBarDisplay } = this.props;

    return (
      <div>
        <Head>
          <title>{title ? `${title} | ${defaultTitle}` : `${defaultTitle} | ${defaultTitleTagLine}`}</title>
        </Head>

        <Header />
        <GlobalStyle />

        {children}
      </div>
    );
  }
}

Page.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  transparentNav: PropTypes.bool,
  isAccountPage: PropTypes.bool
};

export default Page;