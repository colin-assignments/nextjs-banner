import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Fade } from '@material-ui/core';
import Rating from '../Rating';
import ProductImage from './ProductImage';
import { path } from 'ramda';

const styles = (theme) => ({
  previewImage: {
    height: '20em',
    padding: '1em',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      padding: 0,
    },
  },
  vendor: {
    fontSize: '18px !important',
    lineHeight: '1em !important',
    fontWeight: 'bold',
    marginTop: '10px',
    textTransform: 'uppercase',
    letterSpacing: '0.9px',
    color: theme.palette.primary.main,
  },
  title: {
    fontSize: '19px !important',
    fontWeight: 'normal',
    marginTop: '0.25em',
    lineHeight: '1.25em !important',
    letterSpacing: 'normal !important',
    marginBottom: '5px !important',
    [theme.breakpoints.down('sm')]: {
      fontSize: '17px !important',
      fontWeight: 'normal',
      padding: '5px 0px',
      color: '#333',
    },
  },
  cardPreview: {
    position: 'relative',
    display: 'flex',
    width: 'auto',
    margin: '10px 0',
    transition: 'width 1s ease-in-out',
    [theme.breakpoints.down('sm')]: {
      flexWrap: 'wrap',
      margin: 0,
    },
  },
  close: {
    cursor: 'pointer',
    lineHeight: 1,
  },
  closeIcon: {
    color: '#757575',
    margin: '0px 10px',
  },
});

class CardPreview extends Component {
  constructor(props) {
    super(props);
    this.previewCard = React.createRef();
  }

  componentDidMount() {
    let properties = {
      behavior: 'smooth',
      block: 'center',
      inline: 'nearest',
    };
    this.previewCard.current.scrollIntoView(properties);
  }

  render() {
    let { card, classes } = this.props;

    return (
      <Fade in={true} timeout={750}>
        <Card ref={this.previewCard} className={classes.cardPreview}>
          <Grid container className="close">
            <Grid md={4} item>
              <ProductImage product={card} />
            </Grid>

            <Grid md={8} item>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid container item xs={12}>
                    <Grid item xs={11}>
                      <Typography component="h4" className={classes.vendor}>
                        {card.store}
                      </Typography>

                      <Box
                        display="flex"
                        alignItems="center"
                        width="100%"
                        my={1}
                      >
                        <Rating
                          name="read-only"
                          value={path(['reviews', 'rating'], card)}
                          precision={0.5}
                          size="small"
                          readOnly
                        />
                        <Box ml={1} color="#666">
                          ({path(['reviews', 'count'], card)} reviews)
                        </Box>
                      </Box>
                    </Grid>
                    <Grid container item xs={1}>
                      <div className="infodrawer-header-item">
                        <IconButton
                          color="default"
                          aria-label="ElevationChart"
                          onClick={this.props.close}
                        >
                          <CloseIcon />
                        </IconButton>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>

                <Typography className={classes.title} variant="body1">
                  {card.title}
                </Typography>
              </CardContent>
            </Grid>
          </Grid>
        </Card>
      </Fade>
    );
  }
}

CardPreview.defaultProps = {
  card: {},
  close: () => null
};

CardPreview.propTypes = {
  classes: PropTypes.object.isRequired,
  card: PropTypes.object,
  close: PropTypes.func
};

export default withStyles(styles)(CardPreview);
