import styled from 'styled-components';
import { colors } from 'common/constants';
import { transition } from 'common/styles';

export const Card = styled.a`
  background-color: #fff;
  border: 1px solid ${colors.border};
  margin-bottom: 10px;
  cursor: pointer;
  display: block;
  color: #3b4144;
  ${transition('box-shadow')};
  position: relative;
  overflow: hidden;

  &:hover {
    color: #3b4144;
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.1);
    text-decoration: none;
  }
`;

export const Image = styled.div`
  padding-top: 100%;
  background-size: cover;
  background-position: center center;
`;

export const Vendor = styled.div`
  color: #999999;
  font-size: 14px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Title = styled.h4`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Content = styled.div`
  padding: 10px;
`;

export const Price = styled.span`
  color: ${colors.black};
  margin-right: 10px;
  font-size: 14px;
  font-weight: 900;
`;

export const DiscountPrice = styled.span`
  color: ${colors.primary};
  margin-right: 10px;
  font-size: 14px;
`;

export const Bottom = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-top: 15px;
`;

export const Overlay = styled.div`
  align-items: center;
  justify-content: flex-end;
  display: block;
  top: 100%;
  margin-top: -73px;
  position: absolute;
  z-index: 1;
  ${transition('top')};
  width: 100%;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.8);

  ${Card}:hover & {
    top: 0;
    margin-top: 0;
  }
`;

export const FavButton = styled.button`
  position: absolute;
  z-index: 2;
  top: 10px;
  right: 10px;
  background-color: transparent;
  border: 0 none;
  color: ${colors.primary};
  cursor: pointer;
`;
