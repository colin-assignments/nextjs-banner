import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ProductCard, CardPreview, LoadingCard } from './index';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import * as _ from 'lodash';

const styles = (theme) => ({
  previewContainer: {
    margin: '2px 0 0 0',
  },
  preview: {
    width: '100%',
    margin: '1rem .5rem',
  },
  item: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0),
    },
  },
});

class CardSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPreview: false,
      selectedCard: {},
    };
  }

  handleCardClick = (item) => {
    if (!this.props.isMobile) {
      // Initial click handle
      this.props.handleSetClick(this.props.cardSetNumber);
      if (_.isEmpty(this.state.selectedCard)) {
        this.setState({
          showPreview: !this.state.showPreview,
          selectedCard: item,
        });
      } else {
        this.setState({
          selectedCard: item,
        });
      }

      if (_.isEqual(item, this.state.selectedCard)) {
        this.setState({
          showPreview: !this.state.showPreview,
        });
      }
    }
  };

  render() {
    const { cards, classes, selectedSet, cardSetNumber } = this.props;
    const { selectedCard, showPreview } = this.state;

    const isCurrentSetSelected = selectedSet === cardSetNumber;

    const renderCard = (item) => {
      if (item.type == 'product')
        return (
          <div className={classes.item}>
            <ProductCard product={item} />
          </div>
        );
    };

    return (
      <Grid container spacing={1} className={classes.previewContainer}>
        {cards.length > 0 &&
          cards.map((item, index) => {
            return (
              <Grid
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                key={index + 'loaded'}
                onClick={() => this.handleCardClick(item)}
              >
                {renderCard(item)}
              </Grid>
            );
          })}
        {cards.length <= 0 &&
          Array(4)
            .fill({})
            .map((item, index) => {
              return (
                <Grid item xs={6} md={3} key={index + 'loading'}>
                  <LoadingCard />
                </Grid>
              );
            })}
        {showPreview && isCurrentSetSelected && (
          <div item className={classes.preview} id="card-preview">
            <CardPreview
              card={selectedCard}
              close={() => this.setState({ showPreview: false })}
            />
          </div>
        )}
      </Grid>
    );
  }
}

CardSet.defaultProps = {
  cards: [],
  handleSetClick: () => null,
};

CardSet.propTypes = {
  cards: PropTypes.array,
  classes: PropTypes.object.isRequired,
  handleSetClick: PropTypes.func,
};

export default withStyles(styles)(CardSet);
