import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    margin: "30px 10px",
    padding:"20px",
    border:"1px solid rgb(224, 224, 224)",
    borderRadius:"8px",
    background:"transparent",
    boxShadow:"none"
  },

  content: {
    padding: "0px !important",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: "0.875rem",
    fontWeight: "500",
    lineHeight: "24px",
    margin: "10px 0px 0 0",
    color:"black"
  },
  
  desc: {
    color: "rgba(0,0,0,.54)",
    marginBottom: "0",
    fontSize: ".875rem",
    fontWeight: "400",
    lineHeight: "20px"
  },
  footer: {
    justifyContent: "center",
    margin: "10px 0 20px 0",
  },
  btnSelect: {
    fontSize: 16,
    letterSpacing: "0.9px",
    border: "2px solid #142158",
    "&:hover": {
      background: "white",
      border: "2px solid #142158",
      color: "#142158",
    },
  },
});


const Box = (props) => {
  const classes = useStyles();
  const {item:{ img,title, desc} } = props;
  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <img src={"data:image/svg+xml,%3Csvg width='32' height='47' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M16.412.681c-.228-.685-.597-.685-.825 0L.794 45.231c-.228.685.149 1.096.842.917L16 41.124l14.365 5.024c.692.179 1.069-.232.841-.917L16.412.68z' fill='%235063EE' fill-rule='evenodd'/%3E%3C/svg%3E"}/>
        <Typography className={classes.title} color="textSecondary">
          {title}
        </Typography>
       
        <Typography variant="body2" component="p" className={classes.desc}>
          {desc}
        </Typography>
      </CardContent>
    
    </Card>
  );
};

export default Box;
