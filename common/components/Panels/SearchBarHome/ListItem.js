import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'

const ListItemSearchResults = React.memo(({ onClickItem, item, index }) => {
  return (
    <ListItem key={index} button onClick={() => { onClickItem(item) }}>
      <ListItemText classes={{ 'root': 'searchresult-item' }} primary={item.name} disableTypography={true} />
    </ListItem>
  )
});

ListItemSearchResults.propTypes = {
  item: PropTypes.object.isRequired,
  checked: PropTypes.bool
};

export default ListItemSearchResults;