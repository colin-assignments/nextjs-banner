import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import Router from "next/router";
import { withStyles } from "@material-ui/core/styles";
import SimpleSearchForm from "common/components/Forms/SimpleSearchForm";
import { getSearchList, getCategoryList } from "common/store/actions";

const styles = (theme) => ({
  desktopFilters: {
    display: "none",
    maxWidth: "540px",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  mobileFilters: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
  listItemText: {
    fontWeight: 700,
    fontSize: "16px !important",
    lineHeight: "18px !important",
    [theme.breakpoints.down("sm")]: {
      fontSize: "12px !important",
      lineHeight: "16px !important",
    },
  }
});

class SearchBarHomePanel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      inputFocused: false,
    }
  }

  defaultValues = {
    searchText: ""
  };

  gainInputFocus = () => {
    this.setState({
      inputFocused: true
    })
  };

  loseInputFocus = () => {
    // A slight timeout is required so that click actions can occur
    setTimeout(() => {
      this.setState({
        inputFocused: false
      })
    }, 100)
  };

  handleSearchSubmit = (values) => {
    if (values.searchText.length <= 2) {
      return
    }
    // this.props.getSearchResults();
    Router.push(`/search?q=${values.searchText || ""}`);
  };

  handleCheckItem = (e) => {
    let { value } = e.target;
    console.log("Clicked value: ", value);
  };

  render() {
    const { classes, query, subcategories, searchResults } = this.props;

    return (
      <Fragment>
        <div className={classes.desktopFilters}>
          <SimpleSearchForm
            initialValues={this.defaultValues}
            onSubmit={this.handleSearchSubmit}
            onFocus={this.gainInputFocus}
            onBlur={this.loseInputFocus}
            subcategories={subcategories}
          />
        </div>
      </Fragment>
    );
  }
}

SearchBarHomePanel.propTypes = {
  searchLoading: PropTypes.bool,
  disabled: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  categories: ({ entities: { categories } }) => categories,
  search: ({ entities: { search } }) => search,
  searchLoading: ({ app: { searchLoading } }) => searchLoading,
  categoryLoading: ({ app: { categoryLoading } }) => categoryLoading,
});

const mapDispatchToProps = dispatch => {
  return {
    getSearchList: query => dispatch(getSearchList(query)),
    getCategoryList: () => dispatch(getCategoryList())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(SearchBarHomePanel));
