import React, { useState } from "react";
import PropTypes from "prop-types";
import { loadStripe } from '@stripe/stripe-js';
import { withStyles } from "@material-ui/core/styles";
import { Elements } from '@stripe/react-stripe-js';
import { Container, Typography } from "@material-ui/core";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import CheckoutForm from "../../../../common/components/Forms/CheckoutForm";

const stripePromise = loadStripe("pk_test_zerKb9Lm2SoxfcrdjM18XAR800igjb3dKC");

const styles = theme => ({
  container: {
    marginTop: theme.spacing(1),
    paddingBottom: theme.spacing(4)
  },
  root: {
    maxWidth: 500,
    margin: '35vh auto',
  },
  title: {
    fontWeight: 700,
    fontSize: "24px !important",
    lineHeight: "27px !important",
    letterSpacing: "0.6px !important",
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      fontSize: "22px !important",
      lineHeight: "24px !important",
    },
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'flex-start',
  },
  div: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'flex-start',
    justifyContent: 'space-between',
  },
  button: {
    margin: '2em auto 1em',
  },
  list:{
    listStyle:"none",
    padding:0
  },
  listItem:{
    display:"flex",
    "& svg":{
      marginRight:"10px",
      fontSize:20
    }
  },
  name:{
    color:theme.palette.primary.main
  }
});

class SubscriptionPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVerified: false
    };
  }

  initialValues = () => {
    const { user } = this.props;
    if (!user) { return {}; }

    return {
      name: user.name,
      email: user.email,
      password: "",
    };
  }

  render() {
    const { classes, user,selectedPlan:{planName,price,duration,billed} } = this.props;

    return (
      <Container maxWidth="xs" className={classes.container}>
      <Typography className={classes.title} variant="h2">
            Subscribe to a plan
          </Typography>
        <Typography component="h4" className={classes.vendor}>
          Enter your card details. <br/>
          Your subscription will start now.
          <ul className={classes.list}>
            <li className={classes.listItem}> <ArrowForwardIcon/> Total due now ${price}.00</li>
            <li className={classes.listItem}> <ArrowForwardIcon/> Subscribing to  &nbsp;<b className={classes.name}> {planName}  &nbsp; </b>   Plan</li>
          </ul>
        </Typography>
        <Elements stripe={stripePromise}>
          <CheckoutForm onSubmit={this.onSubmit} />
        </Elements>
      </Container>
    );
  }

  preSubmit = ({ name }) => {
    return {
      name: name.trim()
    };
  };

  onSubmit = async params => {
    // setIsSubmitting(true);
    const { stripe, card, formValues } = params;
    console.log("FORM VALUES: ", formValues);
    window.alert("FORM VALUES: ", formValues);
    // const body = this.preSubmit(values);
    // this.props
    //   .updateUserProfile(body)
    //   .then(this.profileUpdateSuccessHandle, this.profileUpdateFailHandle);
  };

  subscriptionUpdateSuccessHandle = () => {
    // this.props.enqueueSnackbar({
    //   message: 'Subscription updated',
    //   options: {
    //     key: new Date().getTime() + Math.random(),
    //     variant: 'success'
    //   },
    // });
  };

  subscriptionUpdateFailHandle = () => {
    // this.props.enqueueSnackbar({
    //   message: 'Something went wrong.',
    //   options: {
    //     key: new Date().getTime() + Math.random(),
    //     variant: 'error'
    //   },
    // });
  };
}

SubscriptionPanel.propTypes = {
  user: PropTypes.object,
};

export default withStyles(styles)(SubscriptionPanel);
