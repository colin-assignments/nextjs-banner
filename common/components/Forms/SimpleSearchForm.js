import React from "react";
import { Form, Field } from "react-final-form";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {searchList} from "./searchList";

const styles = (theme) => ({
  root:{
    padding:"10px",
    [theme.breakpoints.down("sm")]: {
      padding: "10px 0",
    },
  },
  searchField: {
    width: "100%",
    borderRadius: "6px",
    backgroundColor: "#FFFFFF",
    borderColor: "rgba(0, 0, 0, 0.2)",
    "& .MuiInputBase-root": {
      paddingRight: "9px !important",
    },

    "& div": {
      paddingRight: 0,
    },
    "& fieldset": {
      borderColor: "rgba(0, 0, 0, 0.2) !important",
    },
  },
  button: {
    "&:hover": {
      color: theme.palette.secondary.dark,
    },
  },
});


class SearchForm extends React.Component {
  render() {
    const { classes, onSubmit, onFocus, onBlur, initialValues } = this.props;

    const SearchInput = (({ inputProps }) => (
      <Field name={inputProps.name}>
        {({ input }) => (
          <Autocomplete
            freeSolo
            id="free-solo-2-demo"
            disableClearable
            variant="outlined"
            options={searchList.map((option) => option.title)}
            renderInput={(params) => (
              <TextField
                {...params}
                placeholder="Search input"
                variant="outlined"
                className={classes.searchField}
                InputProps={{ ...params.InputProps, type: "search" }}
              />
            )}
          />
        )}
      </Field>
    ))({
      label: "Search",
      inputProps: {
        name: "searchText",
        type: "text",
        placeholder: "Looking for something special?",
      },
    });

    const SubmitForm = ({ handleSubmit }) => (
      <form
        onSubmit={handleSubmit}
        onFocus={onFocus}
        onBlur={onBlur}
        noValidate
        className={classes.root}
        autoComplete="off"
      >
        {SearchInput}
      </form>
    );

    return (
      <Form onSubmit={onSubmit} initialValues={initialValues} >
        {SubmitForm}
      </Form>
    );
  }
}

export default withStyles(styles)(SearchForm);
