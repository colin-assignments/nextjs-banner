import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Form, Field } from 'react-final-form';
import { TextFieldWrapper } from 'common/components/Forms/Wrappers';
import { Button, CircularProgress, InputAdornment } from '@material-ui/core';
import CheckCircleTwoTone from '@material-ui/icons/CheckCircleTwoTone';

import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { checkRequiredFields } from 'common/utils';
import { PaymentCardContainer } from './styles';

const CARD_OPTIONS = {
  style: {
    base: {
      iconColor: '#6772e5',
      color: '#6772e5',
      fontWeight: '500',
      fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
      fontSize: '16px',
      fontSmoothing: 'antialiased',
      ':-webkit-autofill': {
        color: '#fce883',
      },
      '::placeholder': {
        color: '#6772e5',
      },
    },
    invalid: {
      iconColor: '#ef2961',
      color: '#ef2961',
    },
  },
};

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  success: {
    color: '#36b67e',
  },
  linkText: {
    fontSize: '14px !important',
    fontWeight: 900,
    lineHeight: '16.0px !important',
    letterSpacing: '0.8px !important',
    color: '#3f51b5',
    textDecoration: 'uppercase',
    marginBottom: '20px !important',
    cursor: 'pointer',
    [theme.breakpoints.down('sm')]: {
      fontSize: '13px !important',
      lineHeight: '15px !important',
    },
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  planText: {
    textTransform: 'none'
  },
  spinIcon: {
    marginRight: '10px'
  }
}));

const ProfileFormValidate = (values) => {
  const requiredFields = ['name'];
  const errors = checkRequiredFields(requiredFields, values);
  return errors;
};

const CheckoutForm = (props) => {
  const classes = useStyles();
  const stripe = useStripe();
  const elements = useElements();

  // eslint-disable-next-line no-unused-vars
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const handleChange = ({ error }) => {
    if (error) {
      setErrorMessage(error.error.message ?? 'An unknown error occured');
    }
  };

  const handleSubmit = async (values) => {
    const { onSubmit } = props;
    setLoading(true);

    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);

    // Use your card Element with other Stripe.js APIs
    const { error, token } = await stripe.createToken(cardElement);

    if (error) {
      console.log('[error]', error);
    } else {
      const PaymentParams = {
        paymentMethodId: token.id,
        formValues: values,
      };

      onSubmit(PaymentParams);
      setLoading(false);
    }
  };

  const activeStep = 2;

  return (
    <Form
      initialValues={props.initialValues}
      validate={ProfileFormValidate}
      onSubmit={handleSubmit}
      render={({ handleSubmit, submitting }) => {
        return (
          <form className={classes.form} onSubmit={handleSubmit} noValidate>
            <Field
              margin="normal"
              fullWidth
              id="name"
              label="Full Name"
              component={TextFieldWrapper}
              name="name"
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <CheckCircleTwoTone className={classes.success} />
                  </InputAdornment>
                ),
              }}
            />

            <PaymentCardContainer>
              <CardElement
                options={CARD_OPTIONS}
                hidePostalCode="true"
                id="payment"
                onChange={handleChange}
              />
            </PaymentCardContainer>

            <Button
              variant="contained"
              type="submit"
              fullWidth
              className={classes.submit}
              size="large"
              disabled={props.isSubmitting || submitting}
            >
              <Choose>
                <When condition={loading}>
                  <>
                    <If condition={props.isSubmitting || submitting}>
                      <CircularProgress
                        className={classes.spinIcon}
                        size={16}
                      />
                    </If>
                    <span>Subscribing... </span>
                    <span className={classes.planText}>{props.planText}</span>
                  </>
                </When>
                <Otherwise>
                  <Choose>
                    <When condition={activeStep === 2}>
                      <div>
                        <span>Subscribe </span>
                        <span className={classes.planText}>{props.planText}</span>
                      </div>
                    </When>
                    <Otherwise>
                      Next
                    </Otherwise>
                  </Choose>
                </Otherwise>
              </Choose>
            </Button>
          </form>
        );
      }}
    />
  );
};

CheckoutForm.defaultProps = {
  initialValues: {},
  onSubmit: () => null,
  planText: '',
  isSubmitting: false
};

CheckoutForm.propTypes = {
  initialValues: PropTypes.object,
  onSubmit: PropTypes.func,
  planText: PropTypes.string,
  isSubmitting: PropTypes.bool
};

export default CheckoutForm;
