import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "common/routes";
import { ProductCard } from "common/components/ProductCard";
import { Container, Grid, Typography } from "@material-ui/core";
import { LazyLoadComponent } from "react-lazy-load-image-component";
import Slider from "react-slick";

const styles = theme => ({
  desktop: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    },
    [theme.breakpoints.up("md")]: {
      display: "block"
    }
  },
  collectionLayout: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(4, 0, 4),
    "& .slick-arrow:before .slick-arrow:after": {
      fontSize: "25px !important"
    },
    "& .slick-next": {
      right: "-10px"
    },
    "& .slick-prev": {
      left: "-10px"
    }
  },
  productsList: {
    postion: "relative",
    display: "flex",
    overflowX: "auto",
    padding: "20px 0",
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      padding: "10px 0"
    },
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  collectionContent: {
    paddingBottom: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      paddingBottom: theme.spacing(0)
    }
  },
  item: {
    padding: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0)
    }
  },
  title: {
    fontWeight: 600,
    fontSize: "26px !important",
    lineHeight: "32px !important",
    letterSpacing: "1.48px !important",
    color: "#1B2936 !important",
    marginBottom: "10px !important",
    [theme.breakpoints.down("sm")]: {
      fontSize: "20px !important",
      lineHeight: "26px !important"
    }
  },
  description: {
    fontSize: "16px !important",
    lineHeight: "18.0px !important",
    letterSpacing: "0.8px !important",
    color: "#7C7C7C !important",
    marginBottom: "20px !important",
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px !important",
      lineHeight: "16px !important"
    }
  },
  seeMore: {
    fontSize: "14px !important",
    fontWeight: 900,
    lineHeight: "16.0px !important",
    letterSpacing: "0.8px !important",
    color: theme.palette.secondary.main,
    textDecoration: "uppercase",
    marginBottom: "20px !important",
    cursor: "pointer",
    [theme.breakpoints.down("sm")]: {
      fontSize: "13px !important",
      lineHeight: "15px !important"
    }
  }
});

const settings = {
  dots: false,
  arrows: true,
  lazyLoad: true,
  infinite: false,
  speed: 200,
  slidesToShow: 4,
  slidesToScroll: 4,
  swipeToSlide: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4
      }
    }
  ]
};

const CollectionList = props => {
  const { classes, collection, ...rest } = props;

  return (
    <div className={classes.collectionLayout}>
      <Container maxWidth="lg">
        <div className={classes.collectionContent}>
          <Typography component="h2" variant="h2" className={classes.title}>
            {collection.title}
          </Typography>

          <Grid container spacing={2}>
            <Grid container item xs={12}>
              <Grid item xs={8}>
                <Typography component="h4" className={classes.description}>
                  {collection.description}
                </Typography>
              </Grid>
              <Grid container item xs={4} justify="flex-end">
                <Link route="/search?">
                  <Typography component="h4" className={classes.seeMore}>
                    SEE MORE
                  </Typography>
                </Link>
              </Grid>
            </Grid>
          </Grid>

          <Slider {...settings} className={classes.desktop}>
            {collection.entries.map(productID => (
              <Link route="product" params={{ id: productID }} key={productID}>
                <div className={classes.item}>
                  <ProductCard productID={productID} />
                </div>
              </Link>
            ))}
          </Slider>
          <div className={classes.productsList}>
            {collection.entries.map(productID => (
              <Link route="product" params={{ id: productID }} key={productID}>
                <div className={classes.item}>
                  <LazyLoadComponent>
                    <ProductCard productID={productID} />
                  </LazyLoadComponent>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </Container>
    </div>
  );
};

CollectionList.propTypes = {
  classes: PropTypes.object.isRequired,
  collection: PropTypes.object
};

export default withStyles(styles)(CollectionList);
