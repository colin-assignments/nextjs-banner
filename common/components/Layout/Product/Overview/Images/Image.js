import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const styles = theme => ({
  imageContainer: {
    [theme.breakpoints.up("xs")]: {
      height: "25em"
    },
    [theme.breakpoints.up("md")]: {
      height: "30em"
    }
  },
  image: {
    maxHeight: "360px",
    maxWidth: "360px",
    cursor: "pointer",
    "&:hover": {
      transform: "scale(1.5) !important"
    }
  }
});

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false
    };
  }

  render() {
    const { srcUrl, classes } = this.props;
    const { error } = this.state;

    return (
      <div className={classes.imageContainer}>
        &nbsp;
        {!error && (
          <img
            src={srcUrl}
            className={classes.image}
            onError={() => {
              this.setState({ error: true });
            }}
            alt={"Product Image"}
          />
        )}
      </div>
    );
  }
}

Image.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Image);
