import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Slider from "react-slick";
import Image from "./Image";

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
  },
  thumbnails: {
    width: "50px",
    height: "50px",
    display: "flex",
    flexWrap: "wrap"
  },
  thumbnail: {
    width: "50px",
    height: "50px",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: "2px",
    marginBottom: "15px",
    cursor: "pointer",
    "&:hover": {
      boxShadow: theme.palette.primary.main
    }
  }
});

class Images extends React.Component {
  static defaultProps = {
    images: []
  };

  render() {
    const { classes, images } = this.props;

    const settings = {
      fade: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      dots: true,
      arrows: false,
      customPaging: function(i) {
        return (
          <div className={classes.thumbnails}>
            <img className={classes.thumbnail} src={images[i]} alt="" />
          </div>
        );
      },
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false,
            customPaging: function(i) {
              return <div className="dots">&nbsp;</div>;
            }
          }
        }
      ]
    };

    return (
      <div className={classes.root}>
        <Slider {...settings}>
          {images.length > 0 &&
            images.map((image, index) => {
              return (
                <div key={index}>
                  <Image srcUrl={image} />
                </div>
              );
            })}
        </Slider>
      </div>
    );
  }
}

Images.propTypes = {
  classes: PropTypes.object,
  images: PropTypes.array
};

export default withStyles(styles)(Images);
