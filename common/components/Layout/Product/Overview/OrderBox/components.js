import styled from "styled-components";
import { colors } from "common/constants";

const btnWidth = 250; // px

export const Price = styled.div`
  color: ${colors.black};
  font-size: 36px;
`;

export const DiscountPrice = styled.div`
  color: ${colors.primaryColor};
  font-size: 36px;
`;

export const Button = styled.button`
  font-size: 20px !important;
  padding: 0.8em 0 !important;
  width: ${btnWidth}px;
  text-align: center;
`;

export const CartIcon = styled.i`
  font-size: 1.2em;
  margin-right: 0.5em;
`;

export const Meta = styled.div`
  display: flex;
  justify-content: space-between;
  width: ${btnWidth}px;
  margin-left: auto;
`;
