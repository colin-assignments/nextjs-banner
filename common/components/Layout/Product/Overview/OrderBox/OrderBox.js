import { Price, Button, CartIcon, Meta, DiscountPrice } from "./components";

const OrderBox = ({ product, variant }) => {
  const priceMin = (variant && variant.price) || product.price.min;
  const priceMax = (variant && variant.price) || product.price.max;
  const discountPrice =
    (variant && variant.price_discount) || product.promotion.min;
  const weight = (variant && variant.weight) || product.weight;
  const quantity =
    (variant && variant.inventory_quantity) || product.inventory_quantity;

  return (
    <div className="text-right">
      <Price>
        ${priceMin} - ${priceMax}
      </Price>

      <DiscountPrice>${discountPrice}</DiscountPrice>

      <Button className="btn btn-light w-md m-t-10">
        <span>Add to collection</span>
      </Button>

      <Meta className="m-t-10">
        <span>Weight: {weight}g</span>

        <span>{quantity} avaiable</span>
      </Meta>
    </div>
  );
};

export default OrderBox;
