import { Wrapper, StyledButton } from "./components";

const Variant = ({ variants, activeSize, selectSize }) => (
  <Wrapper>
    {variants.map((variant, index) => (
      <StyledButton
        variant="outlined"
        active={activeSize === index}
        disabled={!variant.available}
        onClick={() => selectSize(index)}
      >
        {variant.size}
        {!variant.available && (
          <svg>
            <line x1="0" y1="100%" x2="100%" y2="0" />
          </svg>
        )}
      </StyledButton>
    ))}
  </Wrapper>
);

export default Variant;
