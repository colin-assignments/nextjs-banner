import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Variant from "./Variant";

const styles = theme => ({
  root: {
    flexGrow: 1
  }
});

const Variants = ({
  classes,
  variants,
  activeVariant,
  selectVariant,
  selectedVariant
}) =>
  !!variants && (
    <Fragment>
      {variants.map((variant, index) => (
        <Grid container key={index} className={classes.root} spacing={2}>
          {variant.value.slice(0, 4).map((value, i) => (
            <Grid key={i} item>
              <Variant
                variant={value}
                active={activeVariant === i}
                onClick={() => {
                  selectVariant(i, value);
                  selectedVariant(i, value);
                }}
              />
            </Grid>
          ))}
        </Grid>
      ))}
    </Fragment>
  );

export default withStyles(styles)(Variants);
