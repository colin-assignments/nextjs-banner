import styled, { css } from "styled-components";

const boxShadow = css`
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
`;

export const Root = styled.div`
  border-radius: 2px;
  cursor: pointer;
  padding: 5px;
  width:80px;
  text-align:center;
  /* border: 1px solid ${props => (props.active ? "#333" : "#ccc")}; */
  ${props => props.active && boxShadow}

  &:hover {
    ${boxShadow}
  }
`;

export const Title = styled.div`
  font-size: 16px;
  margin-bottom: 5px;
`;
export const Divider = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  margin: 20px 0;
`;

export const Image = styled.div`
  padding-top: 100%;
  background-size: cover;
  background-position: center center;
`;
