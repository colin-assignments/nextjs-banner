import { Root, Title, Image } from "./components";

const Variant = ({ variant, active, onClick }) => (
  <Root active={active} onClick={onClick}>
    {/* <Title>{variant.title}</Title> */}
    <Image style={{ backgroundImage: `url(${variant.thumbnail_image_url})` }} />
  </Root>
);

export default Variant;
