import React from "react";
import Button from "@material-ui/core/Button";
import { Wrapper, Head, H2 } from "./components";

const Review = () => {
  return (
    <Wrapper>
      <Head>
        <H2>Reviews</H2>
        <Button variant="contained" color="primary">
          Write review
        </Button>
      </Head>
      <p>Be the first person to review this item!</p>
    </Wrapper>
  );
};

export default Review;