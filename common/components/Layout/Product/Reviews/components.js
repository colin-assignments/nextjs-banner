import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
  background: white;
  padding: 20px;
  p {
    font-size: 18px;
  }
`;
export const Head = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const H2 = styled.h2`
  margin: 0;
`;