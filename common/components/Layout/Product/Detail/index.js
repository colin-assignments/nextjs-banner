import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import { Wrapper } from "./components";
import ClipBoardIcon from "common/icons/clipboard";
import MenuIcon from "common/icons/menu";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles(theme => ({
  paper: {
    flex: "none",
    flexShrink: "0",
    overflow: "hidden",
    overflowX: "hidden",
    [theme.breakpoints.down("sm")]: {
      boxShadow: "none"
    },
    [theme.breakpoints.up("lg")]: {
      ...theme.mixins.gutters(),
      padding: "8px 16px 0 8px",
      maxWidth: "1281px",
      maxHeight: "731px",
      margin: "0 auto"
    }
  },
  container: {
    padding: "1em"
  },
  icon: {
    color: '#038f52',
  },
  boxContainer: {
    display: "flex",
    flexDirection: "row",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column"
    },
  },
  boxContainer2: {
    display: "block",
    paddingLeft: "16px !important",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "0px !important",
    },
  },
  heading: {
    fontSize: "18px !important",
    lineHeight: "1em !important",
    fontWeight: "bold",
    marginTop: "5px !important",
    letterSpacing: "0.9px",
    color: "#000000",
    [theme.breakpoints.down("sm")]: {
      fontSize: "13px"
    }
  },
  title: {
    fontWeight: 500,
    fontSize: "16px !important",
    fontWeight: "normal",
    marginTop: "10px !important",
    lineHeight: "1.25em !important",
    letterSpacing: "0.7px !important",
    [theme.breakpoints.down("sm")]: {
      fontSize: "17px !important",
      fontWeight: "normal",
      padding: "5px 0px",
      color: "#333333"
    }
  },
  bullet: { width: 12, height: 12 },
  label: {
    fontSize: "15px !important",
    lineHeight: "1em !important",
    fontWeight: "700",
    marginTop: "10px",
    letterSpacing: "0.9px",
    color: theme.palette.primary.main,
    [theme.breakpoints.down("sm")]: {
      fontSize: "13px !important",
      letterSpacing: "0px",
      fontWeight: "500",
      cursor: "pointer"
    }
  },
}));

export default function Details(props) {
  const classes = useStyles();
  const { product } = props;
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    !!product && (
      <Wrapper>
        <Paper className={classes.paper}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Box component="span" className={classes.boxContainer} m={1}>
                <MenuIcon className={classes.icon} />
                <Box component="span" className={classes.boxContainer2}>
                  <Typography variant="h2" className={classes.heading}>
                    Product description
                  </Typography>
                  <Typography variant="body1" className={classes.title} gutterBottom>
                    {product.title}
                  </Typography>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box component="span" className={classes.boxContainer} m={1}>
                <ClipBoardIcon className={classes.icon} />
                <Box component="span" className={classes.boxContainer2}>
                  <Typography variant="h2" className={classes.heading}>
                    Specification
                  </Typography>
                  <List dense={true}>
                    {product.specification && product.specification.map(key => (
                      <ListItem key={key}>
                        <ListItemIcon>
                          <FiberManualRecordIcon className={classes.bullet} />
                        </ListItemIcon>
                        <ListItemText primary={key.name + ": " + key.value} />
                      </ListItem>
                    ))}
                  </List>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Paper>
      </Wrapper>
    )
  );
}
