import styled from "styled-components";

export const Wrapper = styled.div`
  margin-top: 20px;
  margin-bottom: 50px;
  flex-grow: 1;
  background-color: #f2f2f2;
  .MuiTabs-indicator {
    height: 6px !important;
  }
  .MuiTabs-flexContainer {
    border-bottom: 1px solid #ccc !important;
  }
`;
