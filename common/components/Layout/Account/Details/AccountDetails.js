import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Container, Grid, Typography, Paper } from "@material-ui/core";
import DetailCard from "./DetailCard";
import {
  InfoIcon,
  LoginIcon,
  PaymentIcon,
  StoreIcon,
  PluginIcon,
  GlobalIcon,
} from "../icons";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(4, 0, 4),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(4, 2, 4),
    },
  },
  accountsList: {
    postion: "relative",
    display: "flex",
    overflowX: "auto",
    padding: "20px 0",
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      padding: "10px 0",
    },
  },
  content: {
    textAlign: "center",
  },
  collectionContent: {
    paddingBottom: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      paddingBottom: theme.spacing(0),
    },
  },
  item: {
    width: "100%",
    padding: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0),
    },
  },
  link:{
    color: "green",
    textDecoration:"none",
    fontWeight:600
  },
  title: {
    fontWeight: 600,
    fontSize: "26px !important",
    lineHeight: "32px !important",
    letterSpacing: "1.48px !important",
    color: "#1B2936 !important",
    marginBottom: "10px !important",
    marginLeft: 10,
    [theme.breakpoints.down("sm")]: {
      fontSize: "20px !important",
      lineHeight: "26px !important",
      marginLeft: 0,
    },
  },
  card: {
    flexGrow: 1,
  },
  description: {
    fontSize: "16px !important",
    lineHeight: "18.0px !important",
    letterSpacing: "0.8px !important",
    color: "#1B2936  !important",
    marginBottom: "20px !important",
    marginLeft: 10,
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px !important",
      lineHeight: "16px !important",
      marginLeft: 0,
    },
  },
  seeMore: {
    fontSize: "14px !important",
    fontWeight: 900,
    lineHeight: "16.0px !important",
    letterSpacing: "0.8px !important",
    color: theme.palette.secondary.main,
    textDecoration: "uppercase",
    marginBottom: "20px !important",
    cursor: "pointer",
    [theme.breakpoints.down("sm")]: {
      fontSize: "13px !important",
      lineHeight: "15px !important",
    },
  },
});

const AccountDetails = (props) => {
  const { classes, user, ...rest } = props;

  const detailsList = [
    {
      title: "Personal info",
      description: "Provide personal details and how we can reach you",
      icon: <InfoIcon />,
      args: {
        href: "/account/profile",
      },
    },
    {
      title: "Login & security",
      description: "Update your password and secure your account",
      icon: <LoginIcon />,
      args: {
        href: "/account/login-and-security",
      },
    },
    {
      title: "Payments & subscription",
      description: "Review payments, payouts, coupons, gift cards, and taxes ",
      icon: <PaymentIcon />,
      args: {
        href: "/account/payments-and-subscription",
      },
    },
    {
      title: "Store",
      description: "Provide personal details and how we can reach you",
      icon: <StoreIcon />,
      args: {
        href: "/account/store",
      },
    },
    {
      title: "Integrations & plugins",
      description: "Control connected apps, what you upload and how often",
      icon: <PluginIcon />,
      args: {
        href: "/account/integrations",
      },
    },
    {
      title: "Global preferences",
      description:
        "Set your default product categories, currency, timezone and notifications",
      icon: <GlobalIcon />,
      args: {
        href: "/account/settings",
      },
    },
  ];

  return (
    <Fragment>
      <Container className={classes.root}>
        <Grid container justify="center">
          <Grid container justify="center" item xs={12}>
            <Grid item xs={11}>
              <Typography component="h2" variant="h2" className={classes.title}>
                Account
              </Typography>
              <Typography component="h4" className={classes.description}>
                <b>{user.name}</b>, {user.email}. {" "}
                <a className={classes.link} href="account/profile">Go to profile</a>
              </Typography>
            </Grid>
          </Grid>
          <Grid container item xs={11}>
            <Grid container justify="center" spacing={2}>
              {detailsList.map((item, index) => (
                <Grid key={index} item xs={6} sm={6} md={4}>
                  <div className={classes.item}>
                    <DetailCard detail={item} />
                  </div>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Fragment>
  );
};

AccountDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AccountDetails);
