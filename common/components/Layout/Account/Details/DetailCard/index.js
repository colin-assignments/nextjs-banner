import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

const styles = (theme) => ({
  root: {
    // minWidth: 275,
    minHeight: 195,
    display:"flex",
    alignItems:"center",
    boxShadow:"rgba(0, 0, 0, 0.16) 0px 2px 8px !important",
    [theme.breakpoints.down("xs")]: {
      minHeight: "110px",

    }

  },
  detailContent:{
    padding:24,
    // paddingBottom:"20px !important",
    [theme.breakpoints.down("sm")]: {
      display:"flex",
      flexDirection:"column",
      justifyContent:"center",
      alignItems:"center",
      width:"100%",
      padding:10,
      paddingBottom:"10px !important" 
    },
  },
  title: {
    fontSize: 18,
    fontWeight:700,
    color:"rgb(72, 72, 72)",
    margin:"10px 0 5px 0",
    textDecoration:"none",
    "& svg":{
      fontSize:"1rem",
      marginLeft:"10px",
      color:"green"
    },
    [theme.breakpoints.down("sm")]: {
      "& svg":{
        display:"none"
      }
    },
    [theme.breakpoints.down("xs")]: {
      fontSize:14,
      textAlign:"center",
    }
  },
  description: {
    fontSize: 16,
    color:"rgb(72, 72, 72)",
    [theme.breakpoints.down("sm")]: {
      display:"none"
    }
  },
});

function DetailCard(props) {
  // const classes = useStyles();
  const { detail, classes } = props;

  return (
    <Card className={classes.root}>
      <CardContent className={classes.detailContent}>
        {detail.icon}
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          <a className={classes.title} href={detail.args.href}>

            {detail.title}
            <ArrowForwardIosIcon/>
          </a>
        </Typography>
        <Typography className={classes.description} color="textSecondary">
          {detail.description}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default withStyles(styles)(DetailCard);