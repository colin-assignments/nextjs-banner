import { createMuiTheme } from "@material-ui/core/styles";
import { colors } from "../common/constants";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    background: {
      default: "#d3d3d3"
    },
    primary: {
      light: colors.primaryLight,
      main: colors.primary,
      dark: colors.primaryDark
    },
    secondary: {
      light: colors.secondaryLight,
      main: colors.secondary,
      dark: colors.secondaryDark
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: ["Roboto"].join(",")
  }
});

export default theme;
