# Next JS Banner

## How to use

https://nextjs.org/learn/basics/getting-started/setup

### Using `Setup`

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

Open up  http://localhost:3000
