export { default } from './product'

// import React, { useState } from "react";
// import Router from "next/router";
// import { makeStyles } from "@material-ui/core/styles";
// import { Container, Typography } from "@material-ui/core";
// // Components
// import Page from "../common/components/Page";
// import { SubscriptionPanel } from "../common/components/Panels";
// import PriceBox from "../common/components/PriceBox";

// const user = {
//   name: "Colin Bennett",
//   email: "colin@dropseeker.com",
// };

// const plans = [
//   {
//    planName:"Basic", 
//    price:"50", 
//    duration:"Month", 
//    billed:"Monthly", 
//   },
//   {
//    planName:"Premium", 
//    price:"100", 
//    duration:"Month", 
//    billed:"Monthly", 
//   },
// ]
// const useStyles = makeStyles((theme) => ({
//   container: {
//     [theme.breakpoints.down("md")]: {
//       padding: 0,
//     },
//   },
//   priceBoxContainer:{
//     display:"flex",
//     justifyContent:"center",
//     [theme.breakpoints.down("xs")]: {
//       flexDirection:"column",
//       alignItems:"center"

//     },
//   },
//   center: {
//     marginTop: theme.spacing(6),
//     marginBottom: theme.spacing(9),
//     display: "flex",
//     flexDirection: "column",
//     alignItems: "center",
//   },
 
//   menuContainer: {
//     backgroundColor: theme.palette.background.paper,
//     maxWidth: "20%",
//     [theme.breakpoints.down("sm")]: {
//       display: "none",
//     },
//   },
//   account: {
//     height: "100%",
//     maxWidth: "70%",
//     display: "flex",
//     justifyContent: "center",
//     flexDirection: "column",
//     flexBasis: "80%",
//     [theme.breakpoints.down("sm")]: {
//       maxWidth: "100%",
//       flexBasis: "100%",
//     },
//   },
// }));

// function HomePage() {
//   const classes = useStyles();
//   const [selectedPlan, setSelectedPlan] = useState({});
//   const [active, setActive] = useState(false);
//   const onSubscriptionUpdateSuccess = () => {
//     setPristine(false);

//     const { query } = this.props;
//     const nextQuery = (query.next && query.next.trim()) || "/";

//     setTimeout(() => {
//       Router.push(nextQuery);
//     }, 10);
//   };

//   return (
//     <Page title="Subscription">
//       <Container component="main" maxWidth="lg" className={classes.container}>
//       <div className={classes.priceBoxContainer}>

//         {
//           plans.map(plan => <PriceBox plan={plan} setActive={setActive} setPlan={setSelectedPlan} />)
//         }
//       </div>
//         <div className={classes.center} style={{display:active ? "flex" : "none"}}>
          
//           <SubscriptionPanel
//             onSubscriptionUpdateSuccess={onSubscriptionUpdateSuccess}
//             user={user}
//             selectedPlan={selectedPlan}
//           />
//         </div>
//       </Container>
//     </Page>
//   );
// }

// export default HomePage;
