import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    ".MuiDialogTitle-root":{
      background:"#043353",
    }
  },
  ratings: {
    display: "flex",
    borderBottom:"1px solid #cccccc73",
    "& :last-child":{
        border:"none !important",
    }
  },
  avatar: {
    color: "white",
    marginRight: 20,
  },
  review: {
    overflow:"hidden",
    display: "-webkit-box",
    lineClamp: 2,
    boxOrient: "vertical",
    textOverflow: "ellipsis"
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const Ratings = withStyles(styles)(({ open, handleClose, classes,reviews }) => {
  
  return (
    <Dialog
      fullWidth
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
        Reviews
      </DialogTitle>
      <DialogContent dividers>
      {reviews && reviews.map(rate => 
        <Box
          className={classes.ratings}
          component="fieldset"
          mb={3}
          borderColor="transparent"
        >
          <Avatar className={classes.avatar}>{rate.name}</Avatar>
          <div>
            <Rating
              size="small"
              name="simple-controlled"
              value={rate.rating}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
            />
            <Typography className={classes.review}>
             {rate.review}
            </Typography>
          </div>
        </Box>)}
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
});

export default Ratings;
