import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const boxShadow = css`
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
`;

const Root = styled.div`
  border-radius: 2px;
  cursor: pointer;
  // padding: 5px;
  width:80px;
  text-align:center;
  /* border: 1px solid ${props => (props.active ? '#333' : '#ccc')}; */
  ${props => props.active && boxShadow}

  &:hover {
    ${boxShadow}
  }
`;

const Title = styled.div`
  font-size: 16px;
  margin-bottom: 5px;
`;

const Image = styled.div`
  padding-top: 100%;
  background-size: cover;
  background-position: center center;
`;

const Variant = ({ variant, active, onClick }) => (
  <Root active={active} onClick={onClick}>
    <Title>{variant.title}</Title>
    {variant.thumbnail_image_url
      ? <Image style={{ backgroundImage: `url(${variant.thumbnail_image_url})` }} />
      : null
    }
  </Root>
);

Variant.defaultProps = {
  onClick: () => null,
  variant: {},
  active: true,
};

Variant.propTypes = {
  variant: PropTypes.object,
  onClick: PropTypes.func,
  active: PropTypes.bool,
};

export default Variant;
