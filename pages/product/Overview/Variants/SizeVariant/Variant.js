import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  .Mui-disabled {
    border: 1px solid rgba(0, 0, 0, 0.2) !important;
    color: rgba(0, 0, 0, 0.2);
    border-radius: 0 !important;
  }
  button:last-child {
    margin-right: 0 !important;
  }
`;

const StyledButton = styled(Button)`
  position: relative;
  cursor: pointer;
  display: inline-block;
  margin-right: 10px !important;
  margin-bottom: 10px !important;
  height: 50px !important;
  width: 50px !important;
  min-width: 45px !important;
  padding: 0 !important;
  border-radius: 0 !important;
  line-height: 46px;
  text-align: center;
  color: ${props => (props.active ? '#142158' : 'rgb(51, 51, 51)')};
  border: ${props =>
    props.active
      ? '2px solid #142158'
      : '1px solid rgb(51, 51, 51)'} !important;
  font-size: 18px;
  font-weight: 600;
  margin: 0;

  :hover {
    border: 1px solid #142158 !important;
    background: white;
  }
  svg {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    line {
      stroke: rgba(0, 0, 0, 0.2);
      stroke-width: 1;
    }
  }
`;

const Variant = ({ variants, activeSize, selectSize }) => (
  <Wrapper>
    {variants.map((variant, index) => (
      <StyledButton
        key={index}
        variant="outlined"
        active={activeSize === index}
        disabled={!variant.available}
        onClick={() => selectSize(index)}
      >
        {variant.size}
        {!variant.available && (
          <svg>
            <line x1="0" y1="100%" x2="100%" y2="0" />
          </svg>
        )}
      </StyledButton>
    ))}
  </Wrapper>
);

export default Variant;
