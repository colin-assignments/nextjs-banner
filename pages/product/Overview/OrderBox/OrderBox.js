import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from 'common/constants';

const btnWidth = 250; // px

const Price = styled.div`
  color: ${colors.black};
  font-size: 36px;
`;

const DiscountPrice = styled.div`
  color: ${colors.primary};
  font-size: 36px;
`;

const Button = styled.button`
  font-size: 20px !important;
  padding: 0.8em 0 !important;
  width: ${btnWidth}px;
  text-align: center;
`;

const CartIcon = styled.i`
  font-size: 1.2em;
  margin-right: 0.5em;
`;

const Meta = styled.div`
  display: flex;
  justify-content: space-between;
  width: ${btnWidth}px;
  margin-left: auto;
`;

const OrderBox = ({ product, variant }) => {
  const priceMin = (variant && variant.price) || product.price.min;
  const priceMax = (variant && variant.price) || product.price.max;
  const discountPrice =
    (variant && variant.price_discount) || product.promotion.min;
  const weight = (variant && variant.weight) || product.weight;
  const quantity =
    (variant && variant.inventory_quantity) || product.inventory_quantity;

  return (
    <div className="text-right">
      <Price>
        ${priceMin} - ${priceMax}
      </Price>

      <DiscountPrice>${discountPrice}</DiscountPrice>

      <Button className="btn btn-light w-md m-t-10">
        <span>Add to collection</span>
      </Button>

      <Meta className="m-t-10">
        <span>Weight: {weight}g</span>

        <span>{quantity} avaiable</span>
      </Meta>
    </div>
  );
};

OrderBox.defaultProps = {
  variant: {},
  product: {},
};

OrderBox.propTypes = {
  product: PropTypes.object,
  variant: PropTypes.object,
};

export default OrderBox;
