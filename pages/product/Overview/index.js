import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Reviews from "./Reviews"
import { colors } from '../../../common/constants';

import Images from './Images';
import Variants from './Variants';

export const styles = theme => ({
  paper: {
    flex: 'none',
    flexShrink: '0',
    overflow: 'hidden',
    overflowX: 'hidden',
    paddingBottom: '30px !important',
    [theme.breakpoints.down('sm')]: {
      boxShadow: 'none',
      height: '100%',
    },
    [theme.breakpoints.up('lg')]: {
      ...theme.mixins.gutters(),
      padding: '8px 16px 0 8px',
      maxWidth: '1281px',
      margin: '0 auto'
    }
  },
  content: {
    [theme.breakpoints.down('sm')]: {
      padding: '0 30px !important',
    },
  },
  container: {
    padding: '2px !important',
  },
  title: {
    fontWeight: 500,
    fontSize: '16px !important',
    marginTop: '0.25em',
    lineHeight: '1.25em !important',
    letterSpacing: '0.7px !important',
    marginBottom: '5px !important',
    [theme.breakpoints.down('sm')]: {
      fontSize: '17px !important',
      fontWeight: 'normal',
      padding: '5px 0px',
      color: '#333'
    }
  },
  vendor: {
    fontSize: '20px !important',
    lineHeight: '26px !important',
    fontWeight: 'bold',
    marginTop: '10px',
    letterSpacing: '0.9px',
    color: theme.palette.primary.main,
    cursor: 'pointer',
    [theme.breakpoints.down('sm')]: {
      fontSize: '13px',
      fontWeight: 'bold',
    }
  },
  variant: {
    fontWeight: 400,
    marginTop: '1em',
    fontSize: '14px',
    lineHeight: '18px !important',
    color: 'rgb(153, 153, 153)',
    marginBottom: '0 !important',
    [theme.breakpoints.down('sm')]: {
      fontSize: '12px !important',
      lineHeight: '16px !important'
    }
  },
  variantDesc: {
    fontWeight: 700,
    fontSize: '18px !important',
    lineHeight: '1em !important',
    color: 'rgba(0, 0, 0, 0.9)',
    marginTop: '0 !important',
    marginBottom: '14px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '16px !important',
      lineHeight: '18px !important'
    }
  },
  originalPrice: {
    fontWeight: 400,
    fontSize: '14px',
    marginTop: '1em',
    color: 'rgb(153, 153, 153) !important',
    textDecoration: 'line-through',
    marginBottom: '0 !important',
    [theme.breakpoints.down('sm')]: {
      fontSize: '16px !important',
      lineHeight: '18px !important'
    }
  },
  pricePromotion: {
    fontWeight: 700,
    fontSize: '20px !important',
    lineHeight: '1em !important',
    color: theme.palette.secondary.main,
    marginTop: '0 !important',
    [theme.breakpoints.down('sm')]: {
      fontSize: '18px !important',
      lineHeight: '20px !important'
    }
  },
  pricePromotionPercent: {
    color: theme.palette.secondary.main,
    fontSize: '14px !important',
    lineHeight: '17px !important',
    padding: theme.spacing(1),
  },
  ratingRoot: {
    display: 'flex',
    alignItems: 'center',
  },
  totalRating: {
    display: 'inline-block',
    color: 'rgba(0,0,0,0.5)',
    marginTop: '5px !important',
    textDecoration: 'underline',
    cursor:"pointer"
  },
  button: {
    marginTop: '5px !important',
    marginBottom: '5px !important',
  },
});

const StyledRating = withStyles({
  iconFilled: {
    color: `${colors.secondaryDark}`,
  },
  iconHover: {
    color: '#5b73eb',
  },
})(Rating);

const Divider = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  margin: 20px 0;
`;

class Overview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeVariant: 0,
      activeSize: 0,
      colorVariant: props.product.variants[0].value[0].title,
      inventory_quantity: props.product.variants[0].value[0].inventory_quantity,
      rating: props.product.reviews.rating,
      width: 'Medium',
      open:false,
    };
  }

  handleSelectedVariant = (i, colorVariant) => {
    this.setState({ colorVariant: colorVariant.title, inventory_quantity: colorVariant.inventory_quantity });
  };

  handleSelectedVariant = (i, colorVariant) => {
    this.setState({ colorVariant: colorVariant.title });
  };

  selectVariant = activeVariant => {
    this.setState({ activeVariant });
  };

  selectSize = activeSize => {
    this.setState({ activeSize });
  };

  handleChange = event => {
    this.setState({
      width: event.target.value
    });
  };
  handleCancel = event => {
    this.setState({
      open:false
    });
  };

  render() {
    const { classes, product } = this.props;
    const { activeVariant, colorVariant, inventory_quantity, activeSize, rating } = this.state;

    var colorVariants = product.variants.filter(function (variant) {
      return variant.name.toLowerCase() === 'color';
    });

    return (
      !!product && (
        <Paper className={classes.paper} elevation={1} square>
          <Grid container className={classes.container} spacing={2}>
            <Grid item xs={12} sm={8}>
              <Images images={product.images} />
            </Grid>

            <Grid item xs={12} sm={4} className={classes.content}>
              <Typography component="p" className={classes.vendor}>
                {product.store}
              </Typography>

              <Typography component="p" className={classes.title}>
                {product.title}
              </Typography>

              <div className={classes.ratingRoot}>
                <StyledRating
                  name="customized-color"
                  value={rating}
                  precision={0.5}
                  icon={<FiberManualRecordIcon fontSize="small" style={{ fontSize: 18 }} />}
                  readOnly
                />
                <Box className={classes.totalRating} ml={1} onClick={()=>this.setState({open:true})}>
                  {product.reviews.count} Reviews
                </Box>
                {this.state.open ?<Reviews reviews={product.ratings} open={this.state.open} handleClose={this.handleCancel}/> :null}
              </div>

              <Typography component="p" className={classes.originalPrice}>
                ${product.price.min}
              </Typography>

              <Typography display="inline" component="p" className={classes.pricePromotion}>
                ${product.promotion.min}
              </Typography>
              <Typography display="inline" component="p" className={classes.pricePromotionPercent}>50% off</Typography>
              <Divider />

              {colorVariants.length > 0 && (
                <Fragment>
                  <Typography component="p" className={classes.variant}>
                    Color
                  </Typography>
                  <Typography component="p" className={classes.variantDesc}>
                    {colorVariant}
                  </Typography>
                  <Typography component="p" className={classes.variant}>
                    Stock
                  </Typography>
                  <Typography component="p" className={classes.variantDesc}>
                    {inventory_quantity}
                  </Typography>
                  <Variants
                    variants={colorVariants}
                    activeVariant={activeVariant}
                    selectVariant={this.selectVariant}
                    selectedVariant={this.handleSelectedVariant}
                  />
                  <Divider />
                </Fragment>
              )}

              <Grid container spacing={2}>
                {/* <Grid item xs={12} sm={12} md={5}>
                  <SaveButton product={product} />
                </Grid>
                <Grid item xs={12} sm={12} md={7}>
                  <PrimaryButton block="true">View in AliExpress</PrimaryButton>
                </Grid> */}
              </Grid>

            </Grid>
          </Grid>
        </Paper>
      )  
    );
  }
}

Overview.defaultProps = {
  product: {},
};

Overview.propTypes = {
  classes: PropTypes.object.isRequired,
  product: PropTypes.object
};

// const mapStateToProps = state => ({
//   registerDialogDisplay: state.ui.registerDialogDisplay
// });

// const mapDispatchToProps = dispatch => {
//   return {
//     displayRegisterDialog: bindActionCreators(displayRegisterDialog, dispatch)
//   };
// };

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withStyles(styles)(Overview));

export default withStyles(styles)(Overview);