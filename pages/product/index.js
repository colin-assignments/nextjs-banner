import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Redux
import { compose } from 'redux';
// Style
import styles from './styles';
// Material
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import { withWidth } from '@material-ui/core';
// Component
import Page from '../../common/components/Page';
import Overview from './Overview';
import Detail from './Detail';

import SubscriptionDialog from '../../common/components/Dialogs/SubscriptionDialog';
import ForceUserSubscribeDialog from '../../common/components/Dialogs/ForceUserSubscribeDialog';
import ConfirmDialog from '../../common/components/Dialogs/ConfirmDialog';
import EditProductDialog from '../../common/components/Dialogs/EditProductDialog';
import MoveProductToIntegrationListDialog from '../../common/components/Dialogs/MoveProductToIntegrationListDialog';

const user = {}

const ProductDetail = (props) => {
  const { classes, productDetail, planSelected } = props;

  const[open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);
  const[selectedValue, setSelectedValue] = React.useState();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClickOpen1 = () => {
    setOpen1(true);
  };

  const handleClickOpen2 = () => {
    setOpen2(true);
  };

  const handleClickOpen3 = () => {
    setOpen3(true);
  };

  const handleClickOpen4 = () => {
    setOpen4(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

  return (
    <Page title={productDetail.title}>
      <Container>
        <Overview className={classes.Overview} product={productDetail} />
        {/* <Detail product={productDetail} /> */}
      </Container>

      <Container>
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Open subscription dialog
        </Button>
        <SubscriptionDialog
          user={user}
          isOpen={open}
          handleClose={handleClose}
          planSelected={planSelected}
        />

        <Button variant="outlined" color="primary" onClick={handleClickOpen1}>
          Subscription benfits dialog
        </Button>
        <ForceUserSubscribeDialog isOpen={open1} />

        <Button variant="outlined" color="primary" onClick={handleClickOpen2}>
          Confirm dialog
        </Button>
        <ConfirmDialog
          title="Cancel Subscription Confirm"
          content="Are you sure you want to cancel current subscription?"
          // handleOk={this.handleCancelSubscription}
          // handleCancel={this.onCloseCancelSubscription}
          open={open2}
        />

        <Button variant="outlined" color="primary" onClick={handleClickOpen3}>
          Edit product dialog
        </Button>
        <EditProductDialog
          isOpen={open3}
          handleClose={() =>setOpen3(false)}
        />

        <Button variant="outlined" color="primary" onClick={handleClickOpen4}>
          Move product to integration dialog
        </Button>
        <MoveProductToIntegrationListDialog
          product={productDetail}
          isOpen={open4}
          // handleClose={this.onCloseMoveDialog}
        />
      </Container>
    </Page>
  );

  // render() {
  //   const { classes, productDetail } = this.props;

  //   return (
  //     <Page title={productDetail.title}>
  //       <Container>
  //         <Overview className={classes.Overview} product={productDetail} />
  //         {/* <Detail product={productDetail} /> */}
  //       </Container>

  //       <Button variant="outlined" color="primary" onClick={handleClickOpen}>
  //         Open subscription dialog
  //       </Button>
  //       <SimpleDialog selectedValue={selectedValue} open={open} onClose={handleClose} />
  //       {/* <SubscriptionDialog
  //         user={user}
  //         isOpen={true}
  //         handleClose={this.handleCloseDialog}
  //         planSelected={planSelected}
  //       /> */}
  //     </Page>
  //   );
  // }
};

ProductDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  productDetail: PropTypes.object,
  productId: PropTypes.string,
  getProduct: PropTypes.func,
  width: PropTypes.string.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
)(ProductDetail);

ProductDetail.defaultProps = {
  planSelected: {},
  productDetail: {
    store: "Kailing Watch Store",
    title: "Top Brand Womens Watches Luxury Quartz Casual Watch Women Stainless Steel Mesh Strap Ultra Thin Dial Clock relogio masculino",
    price: {
      max: "4.0",
      min: "1.7"
    },
    promotion: {
      max: "3.16",
      min: "1.34",
      discount: "21",
      // expires_at: ISODate("2018-12-20T18:35:10.650Z"),
      inventory_quantity: 651
    },
    thumbnail_image_url: "https://ae01.alicdn.com/kf/HTB1_GslXjzuK1RjSspeq6ziHVXa4.jpg_350x350.jpg",
    images: [
      "https://ae01.alicdn.com/kf/HTB1_GslXjzuK1RjSspeq6ziHVXa4.jpg_480x480.jpg",
      "https://ae01.alicdn.com/kf/HTB1ENDPaffsK1RjSszgq6yXzpXa9.jpg_480x480.jpg",
      "https://ae01.alicdn.com/kf/HTB1ktI3fkvoK1RjSZFDq6xY3pXar.jpg_480x480.jpg",
      "https://ae01.alicdn.com/kf/HTB1vWFlfyLaK1RjSZFxq6ymPFXaz.jpg_480x480.jpg",
      "https://ae01.alicdn.com/kf/HTB1hbI2fgHqK1RjSZFkq6x.WFXad.jpg_480x480.jpg",
      "https://ae01.alicdn.com/kf/HTB1LSUkXXzsK1Rjy1Xbq6xOaFXaO.jpg_480x480.jpg"
    ],
    variants: [
      {
        name: "color",
        value: [
          {
            id: "201447598",
            title: "Bronze",
            thumbnail_image_url: "https://ae01.alicdn.com/kf/HTB16GEEKkyWBuNjy0Fpq6yssXXaF/Top-Brand-Womens-Watches-Luxury-Quartz-Casual-Watch-Women-Stainless-Steel-Mesh-Strap-Ultra-Thin-Dial.jpg_250x250.jpg",
            inventory_quantity: 0,
            price: ""
          },
        ]
      }
    ],
    reviews: {
      count: 60,
      rating: 4.7,
    },
    ratings:[
      {
        id:1,
        name:"A",
        rating:4,
        review:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget."
      },
      {
        id:2,
        name:"U",
        rating:3,
        review:"Lorem ipsum dolor sit amet"
      },
      {
        id:3,
        name:"X",
        rating:5,
        review:"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      },
      {
        id:4,
        name:"R",
        rating:1,
        review:"Poor"
      },
      {
        id:5,
        name:"C",
        rating:2,
        review:"Good"
      },
    ]
  },
  productId: null,
  getProduct: () => null,
};