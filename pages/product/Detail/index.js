import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Collapse,
  Grid,
  Paper,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { colors } from '../../../common/constants';

const useStyles = makeStyles((theme) => ({
  paper: {
    flex: 'none',
    flexShrink: '0',
    paddingBottom: '50px',
    marginTop: '18px !important',
    [theme.breakpoints.down('sm')]: {
      boxShadow: 'none',
      padding: '0 10px'
    },
    [theme.breakpoints.up('lg')]: {
      ...theme.mixins.gutters(),
      padding: '8px 16px 50px 8px',
      maxWidth: '1281px',
      maxHeight: 'auto',
      margin: '0 auto',
    },
  },
  icon: {
    marginTop: '6px',
    color: theme.palette.primary.main,
    fontSize: '18px !important',
    '& svg': {
      fontSize: '2rem',
    },
  },
  boxContainer: {
    display: 'flex',
    flexDirection: 'row',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  boxContainer2: {
    display: 'lock',
    width: '100%',
    paddingLeft: '16px !important',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '0px !important',
    },
  },
  heading: {
    fontSize: '22px !important',
    lineHeight: '26px !important',
    fontWeight: '700',
    marginTop: '14px !important',
    letterSpacing: '0.7px !important',
    color: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
      fontSize: '13px',
    },
  },
  title: {
    fontWeight: '500',
    fontSize: '16px !important',
    marginTop: '12px !important',
    lineHeight: '1.25em !important',
    letterSpacing: '0.7px !important',
    [theme.breakpoints.down('sm')]: {
      lineHeight: '1.5em !important',
      letterSpacing: '0.9px !important',
      fontSize: '16px !important',
      fontWeight: 'normal',
      padding: '5px 0px',
      color: `${colors.black}`,
    },
  },
  listItem: {
    paddingLeft: 0,
  },
  listText: {
    '& span': {
      fontSize: '15px',
    },
  },
  listIcon: {
    minWidth: 'auto !important',
    marginRight: '10px',
  },
  bullet: {
    width: 8,
    height: 8,
    color: `${colors.black}`,
  },
  label: {
    fontSize: '15px !important',
    lineHeight: '1em !important',
    fontWeight: '700',
    marginTop: '10px',
    letterSpacing: '0.9px',
    color: theme.palette.primary.main,
    [theme.breakpoints.down('sm')]: {
      fontSize: '13px !important',
      letterSpacing: '0px',
      fontWeight: '500',
      cursor: 'pointer',
    },
  },
  tableContainer: {
    marginTop: '30px',
  },
  head: {
    backgroundColor: 'rgb(245, 246, 247)',
    color: 'white',
    '& th': {
      fontSize: '16px',
    },
  },
}));

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  collapseIcon: {
    border: '1px solid #e3e2e2',
    '& svg': {
      width: '0.8em',
      height: '0.8em',
    },
  },
  innerRow: {
    backgroundColor: 'rgb(245, 246, 247)',
    '& th': {
      border: 'none',
    },
  },
});

function createData(date, price, event) {
  return {
    date,
    price,
    event,
    history: [{ date: '2020-01-05', source: 'Compase' }],
  };
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell component="th" scope="row">
          {row.date}
        </TableCell>
        <TableCell align="right">{row.price}</TableCell>
        <TableCell align="right">{row.event}</TableCell>
        <TableCell align="right">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
            className={classes.collapseIcon}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
      </TableRow>
      
      <TableRow className={classes.innerRow}>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={0}>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Source</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history.map((historyRow) => (
                    <TableRow key={historyRow.date}>
                      <TableCell component="th" scope="row">
                        {historyRow.source}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>

    </React.Fragment>
  );
}

const rows = [
  createData('03/13/2020', '$159', 'Listed for sale'),
  createData('03/13/2020', '$237', 'Sold'),
  createData('03/13/2020', '$262', 'Listed for sale'),
  createData('03/13/2020', '$305', 'Posting Removed'),
  createData('03/13/2020', '$356', 'Sold'),
];

Row.defaultProps = {
  row: {},
};

Row.propTypes = {
  row: PropTypes.object,
};

export default function Details(props) {
  const classes = useStyles();
  const { product } = props;
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    !!product && (
      <Paper className={classes.paper} elevation={1} square>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Box component="span" className={classes.boxContainer} m={1}>
              <Box component="span" className={classes.boxContainer2}>
                <Typography variant="h2" className={classes.heading}>
                  Description
                </Typography>
                <Typography
                  variant="body1"
                  className={classes.title}
                  gutterBottom
                >
                  {product.title}
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box component="span" className={classes.boxContainer} m={1}>
              <Box component="span" className={classes.boxContainer2}>
                <Typography variant="h2" className={classes.heading}>
                  Specs
                </Typography>
                
                <List dense={true}>
                  <Grid container >
                    {product.specification &&
                      product.specification.map((key, index) => (
                        <Grid item key={index} xs={12} sm={6} md={6}>
                          <ListItem key={index} className={classes.listItem}>
                            <ListItemIcon className={classes.listIcon}>
                              <FiberManualRecordIcon
                                className={classes.bullet}
                              />
                            </ListItemIcon>
                            <ListItemText
                              primary={key.name + ': ' + key.value}
                              className={classes.listText}
                            />
                          </ListItem>
                        </Grid>
                      ))}
                  </Grid>
                </List>

              </Box>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box component="span" className={classes.boxContainer} m={1}>
              <Box component="span" className={classes.boxContainer2}>
                <Typography variant="h2" className={classes.heading}>
                  Price History
                </Typography>
                <TableContainer
                  className={classes.tableContainer}
                  component={Paper}
                  elevation={0}
                >
                  <Table
                    aria-label="collapsible table"
                    className={classes.table}
                  >
                    <TableHead className={classes.head}>
                      <TableRow>
                        <TableCell>Date</TableCell>
                        <TableCell align="right">Price</TableCell>
                        <TableCell align="right">Event</TableCell>
                        <TableCell />
                      </TableRow>
                    </TableHead>
                    
                    <TableBody>
                      {rows.map((row, index) => (
                        <Row key={index} row={row} />
                      ))}
                    </TableBody>

                    {/* <TableBody>
                      {rows.map((row, index) => (
                        <TableRow key={index}>
                          <TableCell component="th" scope="row">
                            {row.name}
                          </TableCell>
                          <TableCell align="right">{row.price}</TableCell>
                          <TableCell align="right">{row.event}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody> */}



                  </Table>
                </TableContainer>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Paper>
    )
  );
}

Details.defaultProps = {
  product: {},
};

Details.propTypes = {
  product: PropTypes.object,
};
