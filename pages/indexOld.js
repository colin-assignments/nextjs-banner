import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Page from "../common/components/Page";
// import { AccountDetails } from "../common/components/Layout/Account";
import { SearchBarPanel } from "../common/components/Panels";
import {
  Overview,
  Detail,
  CollectionList,
} from "../common/components/Layout/Product";

import {
  Box,
  Container
} from '@material-ui/core';

const product = {
  status: 'published',
  store: 'Rohkn Think',
  title: 'CZCITY Charm Chain Necklace Emerald Green Cubic Zirconia Popular Jewelry 925 Sterling Silver Pendant Necklace for Women Gift',
  description: '',
  tags: [],
  origin_url: 'https://www.aliexpress.com/item/32906237612/32906237612.html',
  specification: [
    { name: 'Brand Name', value: 'CZCITY' },
    { name: 'Gender', value: 'Women' },
    { name: 'Main Stone', value: 'Zircon' },
    { name: 'Metals Type', value: 'Silver' }
  ],
  price: { max: '149.9', min: '149.9' },
  promotion: {
    max: '13.49',
    min: '13.49',
    discount: '91',
    expires_at: '2018-12-20T18:35:10.650000Z',
    inventory_quantity: 1949
  },
  thumbnail_image_url: 'https://ae01.alicdn.com/kf/HTB1L1PcmsUrBKNjSZPxq6x00pXah.jpg_350x350.jpg',
  images: [
    'https://ae01.alicdn.com/kf/HTB1L1PcmsUrBKNjSZPxq6x00pXah.jpg_480x480.jpg',
    'https://ae01.alicdn.com/kf/HTB10yPqmCYTBKNjSZKbq6xJ8pXa4.jpg_480x480.jpg',
    'https://ae01.alicdn.com/kf/HTB1q6x9A2iSBuNkSnhJq6zDcpXaK.jpg_480x480.jpg',
    'https://ae01.alicdn.com/kf/HTB1XzX5mLImBKNjSZFlq6A43FXak.jpg_480x480.jpg',
    'https://ae01.alicdn.com/kf/HTB1sTkbJbSYBuNjSspfq6AZCpXaP.jpg_480x480.jpg',
    'https://ae01.alicdn.com/kf/HTB1WI.Yb56guuRkSmLyq6AulFXam.jpg_480x480.jpg'
  ],
  inventory_quantity: 236,
  reviews: { count: 2872, rating: 4.8 },
  variants: [
    {
      name: 'color', "value": [
        {
          "id": "201447598",
          "title": "Bronze",
          "thumbnail_image_url": "https://ae01.alicdn.com/kf/HTB16GEEKkyWBuNjy0Fpq6yssXXaF/Top-Brand-Womens-Watches-Luxury-Quartz-Casual-Watch-Women-Stainless-Steel-Mesh-Strap-Ultra-Thin-Dial.jpg_250x250.jpg",
          "inventory_quantity": 0,
          "price": ""
        },
        {
          "id": "100005979",
          "title": "Ivory",
          "thumbnail_image_url": "https://ae01.alicdn.com/kf/HTB1G.omXjnuK1RkSmFPq6AuzFXaK/Top-Brand-Womens-Watches-Luxury-Quartz-Casual-Watch-Women-Stainless-Steel-Mesh-Strap-Ultra-Thin-Dial.jpg_250x250.jpg",
          "inventory_quantity": 0,
          "price": ""
        },
        {
          "id": "350686",
          "title": "Brown",
          "thumbnail_image_url": "https://ae01.alicdn.com/kf/HTB1xFdefxjaK1RjSZFAq6zdLFXaU/Top-Brand-Womens-Watches-Luxury-Quartz-Casual-Watch-Women-Stainless-Steel-Mesh-Strap-Ultra-Thin-Dial.jpg_250x250.jpg",
          "inventory_quantity": 0,
          "price": ""
        },
        {
          "id": "350850",
          "title": "Gold",
          "thumbnail_image_url": "https://ae01.alicdn.com/kf/HTB11r3kXfLsK1Rjy0Fbq6xSEXXaA/Top-Brand-Womens-Watches-Luxury-Quartz-Casual-Watch-Women-Stainless-Steel-Mesh-Strap-Ultra-Thin-Dial.jpg_250x250.jpg",
          "inventory_quantity": 0,
          "price": ""
        },
      ]
    },
    {
      name: 'size', "value": [
        {
          "id": "201876815",
          "title": "7-8mm",
          "thumbnail_image_url": "",
          "inventory_quantity": 0,
          "price": ""
        }, 
        {
          "id": "201876816",
          "title": "8-9mm",
          "thumbnail_image_url": "",
          "inventory_quantity": 0,
          "price": ""
        },
        {
          "id": "201876817",
          "title": "9-10mm",
          "thumbnail_image_url": "",
          "inventory_quantity": 0,
          "price": ""
        }
      ]
    }
  ],
  id: '5c2eb9bfbe53e6b103ee27f6',
  type: 'product'
}

const user = {
  name: 'Colin Bennett',
  email: 'colin@dropseeker.com'
  
}

const styles = theme => ({
  container: {
    marginTop: theme.spacing(1),
    paddingBottom: theme.spacing(4)
  }
});

function HomePage() {
  return (
    <Page>
      {/* <AccountDetails user={user} /> */}
        <Box  mt={1} justifyContent="center">
          <SearchBarPanel />
          <Overview product={product} />
          <Detail product={product} />
        </Box>
    </Page>
  );
};

export default withStyles(styles)(HomePage)
