import { useEffect,useState } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid, Hidden} from "@material-ui/core";

// Components
import Page from "../../common/components/Page";
import Filters from "./Filters";
import Banner from './Banner'
import Boxes from "./Boxes";



const HomePage = (props) => {
  const [filters] = useState({show_only: "products_in_usa"})
  useEffect(() => {
  }, [props])

  const handleFilter = () => {

  }
 
  return (
    <Page>

      <Container maxWidth="xl">
        <Grid container spacing={3}>
          <Hidden smDown>
            <Grid item md={4} lg={3} xl={3}>
              <Filters
                defaultFilter={filters}
                onChange={handleFilter}
                // isLoading={isFetching || isLoadMore}
              />
            </Grid>
          </Hidden>
        </Grid>
      </Container>

      
      {/* <Banner user={props.user} />
      <Boxes/> */}
      {/* <CategoryList user={props.user} /> */}
    </Page>
  )
}

export default HomePage
