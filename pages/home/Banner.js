import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Container, Grid, Typography, Button } from "@material-ui/core";
// import { SearchBarHomePanel } from "../../common/components/Panels";
import { colors } from "../../common/constants";

const styles = theme => ({
  heroLayout: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 3),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(1, 0, 1)
    }
  },
  heroContent: {
    display: "flex",
    justifyContent: "space-around",
    marginTop: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      flexWrap: "wrap-reverse"
    }
  },
  heroTitle: {
    color: `${colors.primaryColor}`,
    fontSize: "45px",
    fontWeight: "bolder",
    letterSpacing: "0.7px",
    lineHeight: "48px",
    margin: theme.spacing(16, 0, 2, 0),
    [theme.breakpoints.down("md")]: {
      margin: theme.spacing(20, 0, 0)
    },
    [theme.breakpoints.down("sm")]: {
      // textAlign: "center",
      margin: theme.spacing(4, 0, 2),
      fontSize: "25px !important",
      lineHeight: "28px"
    }
  },
  buttons: {
    marginTop: theme.spacing(3),
    display: "flex",
    justifyContent: "center",
  },
  button: {
    color: "#FFFFFF",
    letterSpacing: 1,
    fontSize: "16px !important",
    backgroundColor: theme.palette.secondary.main,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  secondaryButton: {
    margin: theme.spacing(2, 0, 2, 0),
    justifyContent: "center"
  },
  heroImage: {
    width: "100%",
    maxWidth: "560px",
    minWidth: "340px",
    verticalAlign: "middle",
    [theme.breakpoints.down("xs")]: {
      height: "95%",
      marginTop: theme.spacing(4),
    }
  },
  desc:{
    color:"rgba(0,0,0,0.8)",
    fontSize:"18px",
    fontWeight:"400"
  }
});

const Banner = props => {
  const { classes, user } = props;

  return (
    <div className={classes.heroLayout}>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Typography
              component="h1"
              variant="h2"
              className={classes.heroTitle}
            >
              Discover products your customers will love
            </Typography>
            <Typography
              component="p"
              className={classes.desc}
            >
              Dropseeker is a product aggregator that helps online business owners discover products to sell on their store.
            </Typography>
            {/* <SearchBarHomePanel /> */}
          </Grid>
          <Grid item xs={12} sm={6}>
            <img
              src="https://assets.dropseeker.com/dropseeker-illustration-home.jpg"
              className={classes.heroImage}
              alt="Illustration"
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default withStyles(styles)(Banner);
