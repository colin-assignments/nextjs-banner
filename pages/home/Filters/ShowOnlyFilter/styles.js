const styles = theme => ({
  expansionPanel: {
    background: "transparent",
    boxShadow: "none"
  },
  main: {
    width: "100%"
  },
  button: {
    margin: "5px",
    borderRadius:"40px",
    lineHeight:"32px",
    padding:"0 auto",
    borderColor:"rgb(189, 189, 189)",
    color:"rgb(66,66,66)",
    "&:hover":{
      backgroundColor:"rgb(233, 233, 253)",
      borderColor:"rgb(31, 59, 223) !important",
      color:"rgb(31, 59, 223) !important",

    }
  },
  buttonSelected: {
    borderWidth: "1px !important",
    borderColor:"rgb(31, 59, 223)",
    color:"rgb(31, 59, 223)",
    backgroundColor:"rgb(233, 233, 253)"
  }
});

export default styles;
