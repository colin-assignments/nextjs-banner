import React, { Component } from "react";
import PropTypes from "prop-types";

import memoizeOne from "memoize-one";
import { prop } from "ramda";
import classnames from "classnames";

import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  Button
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";

class ShowOnlyFilter extends Component {
  getValue = memoizeOne(prop("show_only"));

  handleSelectProductsInUSA = () => {
    this.props.onChange({
      show_only: "products_in_usa"
    });
  };

  handleSelectFastDelivery = () => {
    this.props.onChange({
      show_only: "fast_delivery"
    });
  };

  render() {
    const { classes, filters } = this.props;
    const value = this.getValue(filters);

    return (
      <ExpansionPanel defaultExpanded className={classes.expansionPanel}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">Show only</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails>
          <div className={classes.main}>
            <Button
              variant="outlined"
              color="primary"
              className={classnames(classes.button, {
                [classes.buttonSelected]: value === "products_in_usa"
              })}
              onClick={this.handleSelectProductsInUSA}
            >
              Products in the USA
            </Button>
            <Button
              variant="outlined"
              color="primary"
              className={classnames(classes.button, {
                [classes.buttonSelected]: value === "fast_delivery"
              })}
              onClick={this.handleSelectFastDelivery}
            >
              Fast delivery
            </Button>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

ShowOnlyFilter.defaultProps = {};

ShowOnlyFilter.propTypes = {};

export default withStyles(styles)(ShowOnlyFilter);
