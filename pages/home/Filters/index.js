import React, { Component } from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";

import AppliedFilters from "./AppliedFilters";
import DepartmentFilter from "./DepartmentFilter";
import PriceFilter from "./PriceFilter";
import PopularFilter from "./PopularFilter";
import RatingFilter from "./RatingFilter";
import ShowOnlyFilter from "./ShowOnlyFilter";
import styles from "./styles";
import { mergeRight, isEmpty, reject } from "ramda";
import classnames from "classnames";

const listDepartments = [
  {
    name: "Home",
    id: 1
  },
  {
    name: "Electronics",
    id: 2,
    children: [
      {
        name: "Fashion",
        id: 3
      },
      {
        name: "Beauty",
        id: 4
      },
      {
        name: "Personal Care",
        id: 5
      }
    ]
  },
  {
    name: "Fashion",
    id: 3
  },
  {
    name: "Beauty",
    id: 4
  },
  {
    name: "Personal Care",
    id: 5
  },
  {
    name: "Crafts & Gifts",
    id: 6
  },
  {
    name: "Adult",
    id: 7
  }
];

class Filters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filters: this.props.defaultFilter
    };
  }

  handleChange = partOfFilters => {
    const oldFilters = this.state.filters;
console.log(this.state.filters)
    this.setState(
      prev => ({
        filters: mergeRight(prev.filters, partOfFilters)
      }),
      // () => {
      //   this.props.onChange(this.state.filters, partOfFilters).catch(() => {
      //     this.setState({
      //       filters: oldFilters
      //     });
      //   });
      // }
    );
  };

  render() {
    const { classes, isLoading } = this.props;
    const filtersWithoutEmpty = reject(isEmpty, this.state.filters);

    return (
      <div
        className={classnames(classes.filters, {
          [classes.filtersLoading]: isLoading
        })}
      >
        {!isEmpty(filtersWithoutEmpty) && (
          <div className={classes.panel}>
            <AppliedFilters
              filters={this.state.filters}
              onChange={this.handleChange}
              listDepartments={listDepartments}
            />
          </div>
        )}

        <div className={classes.panel}>
          <ShowOnlyFilter
            filters={this.state.filters}
            onChange={this.handleChange}
          />
        </div>

        <div className={classes.panel}>
          <DepartmentFilter
            filters={this.state.filters}
            onChange={this.handleChange}
            listDepartments={listDepartments}
          />
        </div>

        <div className={classes.panel}>
          <PriceFilter
            filters={this.state.filters}
            onChange={this.handleChange}
          />
        </div>

        <div className={classes.panel}>
          <PopularFilter
            filters={this.state.filters}
            onChange={this.handleChange}
          />
        </div>

        <div className={classes.panel}>
          <RatingFilter
            filters={this.state.filters}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

Filters.defaultProps = {
  defaultFilter: {}
};

Filters.propTypes = {
  classes: PropTypes.object.isRequired,
  defaultFilter: PropTypes.object,
  defaultOrder: PropTypes.any
};

export default withStyles(styles)(Filters);
