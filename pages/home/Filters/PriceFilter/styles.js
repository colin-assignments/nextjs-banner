import { withStyles } from "@material-ui/core/styles";
import {Slider} from "@material-ui/core";

const styles = theme => ({
  expansionPanel: {
    background: "transparent",
    boxShadow: "none"
  },
  main: {
    width: "100%"
  },
  result: {
    alignItems: 'center',
    display: 'flex',
    marginBottom: '5px',
    justifyContent:"space-between"
  },
  input:{
    width:"49%"
  }
});

export const AirbnbSlider = withStyles({
  root: {
    color: '#5063F0',
    height: 3,
    padding: '13px 0',
  },
  thumb: {
    height: 27,
    width: 27,
    backgroundColor: '#fff',
    border: '1px solid currentColor',
    marginTop: -12,
    marginLeft: -13,
    boxShadow: '#ebebeb 0 2px 2px',
    '&:focus, &:hover, &$active': {
      boxShadow: '#ccc 0 2px 3px 1px',
    },
    '& .bar': {
      // display: inline-block !important;
      height: 9,
      width: 1,
      backgroundColor: 'currentColor',
      marginLeft: 1,
      marginRight: 1,
    },
  },
  active: {},
  track: {
    height: 3,
  },
  rail: {
    color: '#d8d8d8',
    opacity: 1,
    height: 3,
  },
})(Slider);

export default styles;
