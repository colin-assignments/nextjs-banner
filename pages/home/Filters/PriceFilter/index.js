import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  Slider,
  TextField
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";
import {AirbnbSlider} from "./styles";
import memoizeOne from "memoize-one";
import { always, either, head, last, propOr } from "ramda";

const MIN_PRICE = 0;
const MAX_PRICE = 1000;

const getValue = memoizeOne(filters => {
  const min = either(
    propOr(MIN_PRICE, "from_price"),
    always(MIN_PRICE)
  )(filters);
  const max = either(propOr(MAX_PRICE, "to_price"), always(MAX_PRICE))(filters);
  return [Number(min), Number(max)];
});

const AirbnbThumbComponent = (props) =>  {
  return (
    <span {...props}>
      <span className="bar" />
      <span className="bar" />
      <span className="bar" />
    </span>
  );
}
class PriceFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: getValue(props.filters)
    };
  }

  static getDerivedStateFromProps(props, state) {
    const [min, max] = getValue(props.filters);

    if (min !== state.prevMin || max !== state.prevMax) {
      return {
        value: [min, max],
        prevMin: min,
        prevMax: max
      };
    }

    return null;
  }

  handleChange = (event, newValue) => {
    console.log(newValue)
    this.setState({
      value: newValue
    });
  };

  handleChangeCommitted = () => {
    const [from_price, to_price] = this.state.value;

    this.props.onChange({
      from_price,
      to_price
    });
  };
  handleMinInputChange = (e) => {
    console.log(e.target.value)
    this.setState({
      value: [e.target.value,last(this.state.value)]
    });
    this.props.onChange(
      e.target.value,
      MAX_PRICE
    );
  }
  handleMaxInputChange = (e) => {
    console.log(e.target.value)
    this.setState({
      value: [head(this.state.value), e.target.value]
    });
    this.props.onChange(
      MIN_PRICE,
      e.target.value
    );
  }
  

  render() {
    const { classes, filters } = this.props;

    return (
      <ExpansionPanel defaultExpanded className={classes.expansionPanel}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">Price</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails>
          <div className={classes.main}>
            <div className={classes.result}>
              <TextField id="outlined-basic" className={classes.input} onChange={this.handleMinInputChange} value={head(this.state.value)} variant="outlined" />
              <TextField id="outlined-basic" className={classes.input} onChange={this.handleMaxInputChange} value={last(this.state.value)} variant="outlined" />
            
            </div>
            <AirbnbSlider
              value={this.state.value}
              onChange={this.handleChange}
              onChangeCommitted={this.handleChangeCommitted}
              ThumbComponent={AirbnbThumbComponent}
              getAriaLabel={(index) => (index === 0 ? 'Minimum price' : 'Maximum price')}
              defaultValue={[MIN_PRICE, MAX_PRICE]}
              step={1}
            />
            {/* <Slider
              value={this.state.value}
              onChange={this.handleChange}
              onChangeCommitted={this.handleChangeCommitted}
              valueLabelDisplay="off"
              getAriaValueText={a => a}
              min={MIN_PRICE}
              step={1}
              max={MAX_PRICE}
            /> */}
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

PriceFilter.defaultProps = {};

PriceFilter.propTypes = {};

export default withStyles(styles)(PriceFilter);
