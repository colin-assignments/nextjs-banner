import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  List,
  ListItem,
  Typography,
  ListItemIcon,
  Checkbox,
  ListItemText,
  Box
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";
import Rating from "../../../../common/components/Rating";
import memoizeOne from "memoize-one";
import {
  append,
  curryN,
  equals,
  lensProp,
  over,
  pipe,
  propOr,
  reject
} from "ramda";

class PopularFilter extends Component {
  getSelectedPopular = memoizeOne(filters => {
    return propOr([], "popular", filters);
  });

  handleSelect = popularId => {
    const lens = lensProp("popular");
    const newPopular = over(lens, append(popularId), this.props.filters);
    this.props.onChange(newPopular);
  };

  handleRemove = popularId => {
    const lens = lensProp("popular");
    const newPopular = over(
      lens,
      reject(pipe(String, equals(String(popularId)))),
      this.props.filters
    );
    this.props.onChange(newPopular);
  };

  isSelected = popular => {
    const selected = this.getSelectedPopular(this.props.filters);

    return Boolean(
      selected.filter(id => String(id) === String(popular.id)).length
    );
  };

  handleToggleSelect = curryN(2, popular => {
    const isSelected = this.isSelected(popular);

    if (isSelected) {
      this.handleRemove(popular.id);
    } else {
      this.handleSelect(popular.id);
    }
  });

  render() {
    const { classes } = this.props;

    return (
      <ExpansionPanel defaultExpanded className={classes.expansionPanel}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">Popular</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails>
          <div className={classes.main}>
            <List>
              <ListItem
                role={undefined}
                dense
                button
                onClick={this.handleToggleSelect({ id: 1 })}
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    tabIndex={-1}
                    disableRipple
                    checked={this.isSelected({
                      id: 1
                    })}
                  />
                </ListItemIcon>
                <ListItemText primary="Breakfast included" />
              </ListItem>

              <ListItem
                role={undefined}
                dense
                button
                onClick={this.handleToggleSelect({ id: 2 })}
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    tabIndex={-1}
                    disableRipple
                    checked={this.isSelected({
                      id: 2
                    })}
                  />
                </ListItemIcon>
                <ListItemText
                  primary={
                    <Box display="flex" alignItems="center">
                      <Rating value={4} readOnly />
                      <span> & up</span>
                    </Box>
                  }
                />
              </ListItem>

              <ListItem
                role={undefined}
                dense
                button
                onClick={this.handleToggleSelect({ id: 3 })}
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    tabIndex={-1}
                    disableRipple
                    checked={this.isSelected({
                      id: 3
                    })}
                  />
                </ListItemIcon>
                <ListItemText primary="4 stars" />
              </ListItem>
            </List>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

PopularFilter.defaultProps = {};

PopularFilter.propTypes = {};

export default withStyles(styles)(PopularFilter);
