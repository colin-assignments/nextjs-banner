import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  List,
  ListItem,
  Typography,
  ListItemIcon,
  Checkbox,
  ListItemText,
  Box,
  Radio,
  RadioGroup,
  FormControlLabel
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";
import Rating from "../../../../common/components/Rating";
import memoizeOne from "memoize-one";
import { prop } from "ramda";

class RatingFilter extends Component {
  getValue = memoizeOne(filters => {
    return prop("rating", filters);
  });

  handleChange = event => {
    this.props.onChange({
      rating: event.target.value
    });
  };

  render() {
    const { classes, filters } = this.props;
    const value = this.getValue(filters);

    return (
      <ExpansionPanel defaultExpanded className={classes.expansionPanel}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">Product rating</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails>
          <div className={classes.main}>
            <RadioGroup value={value} onChange={this.handleChange}>
              <List>
                <ListItem role={undefined} dense button>
                  <ListItemText
                    primary={
                      <FormControlLabel
                        className={classes.formControlLabel}
                        value="5"
                        control={<Radio />}
                        label={<Rating value={5} readOnly />}
                      />
                    }
                  />
                </ListItem>

                <ListItem role={undefined} dense button>
                  <ListItemText
                    primary={
                      <FormControlLabel
                        className={classes.formControlLabel}
                        value="4"
                        control={<Radio />}
                        label={
                          <Box display="flex" alignItems="center">
                            <Rating value={4} readOnly />
                            <span> & up</span>
                          </Box>
                        }
                      />
                    }
                  />
                </ListItem>

                <ListItem role={undefined} dense button>
                  <ListItemText
                    primary={
                      <FormControlLabel
                        className={classes.formControlLabel}
                        value="3"
                        control={<Radio />}
                        label={
                          <Box display="flex" alignItems="center">
                            <Rating value={3} readOnly />
                            <span> & up</span>
                          </Box>
                        }
                      />
                    }
                  />
                </ListItem>

                <ListItem role={undefined} dense button>
                  <ListItemText
                    primary={
                      <FormControlLabel
                        className={classes.formControlLabel}
                        value="2"
                        control={<Radio />}
                        label={
                          <Box display="flex" alignItems="center">
                            <Rating value={2} readOnly />
                            <span> & up</span>
                          </Box>
                        }
                      />
                    }
                  />
                </ListItem>
              </List>
            </RadioGroup>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

RatingFilter.defaultProps = {};

RatingFilter.propTypes = {};

export default withStyles(styles)(RatingFilter);
