const styles = theme => ({
  expansionPanel: {
    background: "transparent",
    boxShadow: "none"
  },
  main: {
    width: "100%"
  },
  formControlLabel: {
    width: "100%"
  }
});

export default styles;
