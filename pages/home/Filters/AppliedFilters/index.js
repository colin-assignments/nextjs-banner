import React, { Component } from "react";
import PropTypes from "prop-types";

import memoizeOne from "memoize-one";
import {
  pipe,
  propOr,
  map,
  find,
  propEq,
  applySpec,
  prop,
  always,
  curry,
  curryN,
  flatten,
  reject,
  equals,
  chain,
  zipObj
} from "ramda";

import { Typography, Chip } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";

const objFromListWith = curry((fn, list) => chain(zipObj, map(fn))(list));

class AppliedFilters extends Component {
  getListDepartments = memoizeOne((filters, listDepartments) => {
    const mapById = objFromListWith(prop("id"), listDepartments);

    return pipe(
      propOr([], "departments"),
      map(id => mapById[id]),
      map(
        applySpec({
          type: always("departments"),
          label: prop("name"),
          value: prop("id")
        })
      )
    )(filters);
  });

  getPopular = memoizeOne(() => {
    return null;
  });

  getSingleValue = curry((type, label, filters) => {
    if (filters[type]) {
      return {
        type,
        label,
        value: filters[type]
      };
    }
  });

  getFromPrice = memoizeOne(this.getSingleValue("from_price", "From price"));
  getShowOnly = memoizeOne(this.getSingleValue("show_only", "Show only"));
  getToPrice = memoizeOne(this.getSingleValue("to_price", "To price"));
  getRating = memoizeOne(this.getSingleValue("rating", "Rating"));

  handleRemove = curryN(2, ({ type, value }) => {
    if (type === "departments") {
      return this.props.onChange({
        departments: reject(
          pipe(String, equals(String(value))),
          this.props.filters.departments
        )
      });
    }

    if (
      type === "from_price" ||
      type === "to_price" ||
      type === "rating" ||
      type === "show_only"
    ) {
      return this.props.onChange({
        [type]: ""
      });
    }

    if (type === "popular") {
      return this.props.onChange({
        popular: []
      });
    }
  });

  render() {
    const { filters, classes, listDepartments } = this.props;
    const departments = this.getListDepartments(filters, listDepartments);
    const popular = this.getPopular(filters);
    const fromPrice = this.getFromPrice(filters);
    const toPrice = this.getToPrice(filters);
    const rating = this.getRating(filters);
    const showOnly = this.getShowOnly(filters);
    const list = flatten([
      departments,
      popular ? popular : [],
      fromPrice ? fromPrice : [],
      toPrice ? toPrice : [],
      rating ? rating : [],
      showOnly ? showOnly : [],
    ]);

    return (
      <div className={classes.appliedFilters}>
        <Typography variant="h6">Applied Filters</Typography>

        <div className={classes.list}>
          {list.map(item => (
            <Chip
              key={item.type + item.value}
              label={item.label}
              onDelete={this.handleRemove(item)}
              className={classes.chip}
            />
          ))}
        </div>
      </div>
    );
  }
}

AppliedFilters.defaultProps = {};

AppliedFilters.propTypes = {};

export default withStyles(styles)(AppliedFilters);
