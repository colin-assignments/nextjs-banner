const styles = theme => ({
  appliedFilters: {},
  list: {
    margin: "15px 0"
  },
  chip: {
    margin: 5
  }
});

export default styles;
