const styles = theme => ({
  expansionPanel: {
    background: "transparent",
    boxShadow: "none"
  },
  main: {
    width: "100%"
  },
  form: {
    display: "block",
    width: "100%"
  },
  searchIcon: {
    padding: "5px !important"
  },
  nestedList: {
    paddingLeft: theme.spacing(3)
  }
});

export default styles;
