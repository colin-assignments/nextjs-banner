import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  Checkbox,
  Collapse,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { withStyles } from "@material-ui/core/styles";

import styles from "./styles";
import { curryN } from "ramda";

class DepartmentItem extends Component {
  state = {
    isShowNested: true
  };

  maybeToggleNested = e => {
    e.stopPropagation();

    if (Array.isArray(this.props.department.children)) {
      this.setState(prev => ({
        isShowNested: !prev.isShowNested
      }));
    }
  };

  handleToggleSelect = curryN(2, department => {
    const isSelected = this.isSelected(department);

    if (isSelected) {
      this.props.onRemove(department.id);
    } else {
      this.props.onSelect(department.id);
    }
  });

  isSelected = department =>
    Boolean(
      this.props.listSelected.filter(id => String(id) === String(department.id))
        .length
    );

  render() {
    const { department, classes } = this.props;
    const children = department.children;

    return (
      <React.Fragment>
        <ListItem
          key={department.id}
          role={undefined}
          dense
          button
          onClick={this.handleToggleSelect(department)}
        >
          <ListItemIcon>
            <Checkbox
              edge="start"
              tabIndex={-1}
              disableRipple
              checked={this.isSelected(department)}
            />
          </ListItemIcon>
          <ListItemText id={department.id} primary={department.name} />

          {Array.isArray(children) &&
            (this.state.isShowNested ? (
              <ExpandLess onClick={this.maybeToggleNested} />
            ) : (
              <ExpandMore onClick={this.maybeToggleNested} />
            ))}
        </ListItem>

        {Array.isArray(children) && (
          <Collapse in={this.state.isShowNested} component="li" timeout="auto">
            <List disablePadding className={classes.nestedList}>
              {children.map(department => {
                return (
                  <ListItem
                    key={department.id}
                    role={undefined}
                    dense
                    button
                    onClick={this.handleToggleSelect(department)}
                  >
                    <ListItemIcon>
                      <Checkbox
                        edge="start"
                        tabIndex={-1}
                        disableRipple
                        checked={this.isSelected(department)}
                      />
                    </ListItemIcon>
                    <ListItemText
                      id={department.id}
                      primary={department.name}
                    />
                  </ListItem>
                );
              })}
            </List>
          </Collapse>
        )}
      </React.Fragment>
    );
  }
}

DepartmentItem.defaultProps = {};

DepartmentItem.propTypes = {};

export default withStyles(styles)(DepartmentItem);
