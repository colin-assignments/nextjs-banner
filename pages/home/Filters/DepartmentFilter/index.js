import React, { Component } from "react";
import PropTypes from "prop-types";

import memoizeOne from "memoize-one";
import {
  over,
  lensProp,
  propOr,
  append,
  equals,
  reject,
  pipe,
  filter,
  includes,
  propSatisfies,
  toLower
} from "ramda";

import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  IconButton,
  InputAdornment,
  Typography,
  List
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Form, Field } from "react-final-form";

import { withStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import styles from "./styles";
import DepartmentItem from "./DepartmentItem";
import { TextFieldWrapper } from "../../../../common/components/Forms/Wrappers";

class DepartmentFilter extends Component {
  state = {
    filterName: ""
  };

  getListDepartment = memoizeOne((filterName, departments) => {
    return filter(
      propSatisfies(pipe(toLower, includes(filterName)), "name"),
      departments
    );
  });

  getSelectedDepartment = memoizeOne(filters => {
    return propOr([], "departments", filters);
  });

  handleSelect = departmentId => {
    const lens = lensProp("departments");
    const newDepartments = over(lens, append(departmentId), this.props.filters);
    this.props.onChange(newDepartments);
  };

  handleRemove = departmentId => {
    const lens = lensProp("departments");
    const newDepartments = over(
      lens,
      reject(pipe(String, equals(String(departmentId)))),
      this.props.filters
    );
    this.props.onChange(newDepartments);
  };

  handleFilterDepartment = values => {
    this.setState({
      filterName: propOr("", "name", values)
    });
  };

  render() {
    const { classes, filters, listDepartments } = this.props;
    const selected = this.getSelectedDepartment(filters);
    const departments = this.getListDepartment(
      this.state.filterName,
      listDepartments
    );

    return (
      <ExpansionPanel defaultExpanded className={classes.expansionPanel}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">Department</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails>
          <div className={classes.main}>
            <Form
              onSubmit={this.handleFilterDepartment}
              render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit} className={classes.form}>
                  <Field
                    fullWidth
                    variant="outlined"
                    placeholder="Search..."
                    type="text"
                    size="small"
                    component={TextFieldWrapper}
                    name="name"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <IconButton
                            type="submit"
                            className={classes.searchIcon}
                          >
                            <SearchIcon />
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                </form>
              )}
            />

            <List>
              {departments.map(department => (
                <DepartmentItem
                  key={department.id}
                  department={department}
                  listSelected={selected}
                  onSelect={this.handleSelect}
                  onRemove={this.handleRemove}
                />
              ))}
            </List>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

DepartmentFilter.defaultProps = {};

DepartmentFilter.propTypes = {};

export default withStyles(styles)(DepartmentFilter);
