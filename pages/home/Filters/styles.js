const styles = theme => ({
  filters: {
    background: "#fff",
    border: "1px solid #e0e0e0",
    padding: theme.spacing(3),

    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
    },
  },
  filtersLoading: {
    pointerEvents: "none",
    opacity: 0.7
  },
  panel: {
    borderBottom: "1px solid #e0e0e0",
    padding: "16px 0"
  }
});

export default styles;
