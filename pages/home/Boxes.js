import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid} from "@material-ui/core";
import Box from "../../common/components/Box"
// import img1 from "../../public/static/images/1.svg";
// import img2 from "../../public/static/images/2.svg";
// import img3 from "../../public/static/images/3.svg";

const useStyles =  makeStyles({

    heading: {
      textAlign:"center",
      fontSize: "1.5rem",
      fontWeight: "400",
      lineHeight:" 32px",
      margin:"30px auto"
    },
    
    
  });

  const items = [
    {
      // img:img1,
      title:"Title 1",
      desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
    {
      // img:img2,
      title:"Title 2",
      desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
    {
      // img:img3,
      title:"Title 3",
      desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
  ]

export default  () => {
  const classes = useStyles();

    return (
        <Container maxWidth="lg">
            <Grid container direction="row" justify="center" alignItems="center" >
            <Grid item xs={12}>
                <h3 className={classes.heading}>What can dropseeker do for you</h3>
            </Grid>
            {items && items.map(item => <Grid item xs={12} sm={12} md={4} spacing={3}> <Box item={item}/> </Grid>)}
            
            </Grid>
            
        </Container>
    )
}