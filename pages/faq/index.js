import React, {useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Accordion } from "@material-ui/core";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import Header from "../../common/components/Page/Header"
const useStyles = makeStyles((theme) => ({
  root: {
    width: "80%",
    margin:"80px auto",
    "& .MuiAccordionSummary-root":{
      minHeight:"70px"
    },
    "& .MuiPaper-elevation1":{
      boxShadow:"none",
    },
    "& .MuiAccordionSummary-content.Mui-expanded":{
      borderBottom:"1px solid rgba(0, 0, 0, 0.12)",
      margin:"10px 0",
      padding:"10px 0",
    },
    "& .MuiAccordion-root:before":{
     display:"none"
    },
  },

  heading: {
    display:"flex",
    alignItems:"center",
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  icon:{
    color: theme.palette.primary.main,
    marginRight:20,
    fontSize:"35px"
  }
}));

const faq = () => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(1)
  return (
    <>
    <Header/>
    <div className={classes.root}>
      <Accordion 
        expanded={expanded === 1}
        onChange={()=>setExpanded(1)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          
        >
          <Typography className={classes.heading}><LiveHelpIcon className={classes.icon}/> Question 1</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
         expanded={expanded === 2}
        onChange={()=>setExpanded(2)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}><LiveHelpIcon className={classes.icon}/> Question 2</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
         expanded={expanded === 3}
        onChange={()=>setExpanded(3)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3a-content"
          id="panel3a-header"
        >
          <Typography className={classes.heading}><LiveHelpIcon className={classes.icon}/>
            Question 3
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
         expanded={expanded === 4}
        onChange={()=>setExpanded(4)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4a-content"
          id="panel4a-header"
        >
          <Typography className={classes.heading}>
          <LiveHelpIcon className={classes.icon}/>
            Question 4
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion 
         expanded={expanded === 5}
        onChange={()=>setExpanded(5)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel5a-content"
          id="panel5a-header"
        >
          <Typography className={classes.heading}>
          <LiveHelpIcon className={classes.icon}/>
            Question 5
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
    </>
  );
};

export default faq;
